<?xml version = '1.0' encoding = 'UTF-8'?>
<trigger xmlns="http://xmlns.oracle.com/jdeveloper/1111/offlinedb">
  <ID class="oracle.javatools.db.IdentifierBasedID">
    <identifier class="java.lang.String">1b49b7bb-4473-4f03-ba19-cae935b6b851</identifier>
  </ID>
  <name>CONTACTS_TRG</name>
  <baseType>TABLE</baseType>
  <code>BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.CONTACT_ID IS NULL THEN
      SELECT CONTACTS_SEQ.NEXTVAL INTO :NEW.CONTACT_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</code>
  <enabled>true</enabled>
  <events>
    <event>INSERT</event>
  </events>
  <schema>
    <name>SCHEMA1</name>
  </schema>
  <source>CREATE TRIGGER CONTACTS_TRG 
BEFORE INSERT ON CONTACTS 
FOR EACH ROW 
BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.CONTACT_ID IS NULL THEN
      SELECT CONTACTS_SEQ.NEXTVAL INTO :NEW.CONTACT_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</source>
  <statementLevel>false</statementLevel>
  <tableID class="oracle.javatools.db.IdentifierBasedID">
    <name>CONTACTS</name>
    <identifier class="java.lang.String">643f7bd1-1411-492b-9fa0-0711281188da</identifier>
    <schemaName>SCHEMA1</schemaName>
    <type>TABLE</type>
  </tableID>
  <timing>BEFORE</timing>
</trigger>
