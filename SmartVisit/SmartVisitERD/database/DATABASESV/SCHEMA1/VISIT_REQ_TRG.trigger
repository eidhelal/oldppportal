<?xml version = '1.0' encoding = 'UTF-8'?>
<trigger xmlns="http://xmlns.oracle.com/jdeveloper/1111/offlinedb">
  <ID class="oracle.javatools.db.IdentifierBasedID">
    <identifier class="java.lang.String">bf034ae3-159e-4e1a-83b2-6ff8ebf2a4c2</identifier>
  </ID>
  <name>VISIT_REQ_TRG</name>
  <baseType>TABLE</baseType>
  <code>BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.VISIT_ID IS NULL THEN
      SELECT VISIT_REQ_SEQ.NEXTVAL INTO :NEW.VISIT_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</code>
  <enabled>true</enabled>
  <events>
    <event>INSERT</event>
  </events>
  <schema>
    <name>SCHEMA1</name>
  </schema>
  <source>CREATE TRIGGER VISIT_REQ_TRG 
BEFORE INSERT ON VISIT_REQ 
FOR EACH ROW 
BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.VISIT_ID IS NULL THEN
      SELECT VISIT_REQ_SEQ.NEXTVAL INTO :NEW.VISIT_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</source>
  <statementLevel>false</statementLevel>
  <tableID class="oracle.javatools.db.IdentifierBasedID">
    <name>VISIT_REQ</name>
    <identifier class="java.lang.String">5c023535-9c3f-4f67-b588-0f6ba81a921b</identifier>
    <schemaName>SCHEMA1</schemaName>
    <type>TABLE</type>
  </tableID>
  <timing>BEFORE</timing>
</trigger>
