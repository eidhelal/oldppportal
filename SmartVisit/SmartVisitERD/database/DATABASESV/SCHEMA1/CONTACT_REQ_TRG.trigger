<?xml version = '1.0' encoding = 'UTF-8'?>
<trigger xmlns="http://xmlns.oracle.com/jdeveloper/1111/offlinedb">
  <ID class="oracle.javatools.db.IdentifierBasedID">
    <identifier class="java.lang.String">0a5850a9-e604-41c0-97bb-5614abdb9bfb</identifier>
  </ID>
  <name>CONTACT_REQ_TRG</name>
  <baseType>TABLE</baseType>
  <code>BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.CONTACT_REQ_ID IS NULL THEN
      SELECT CONTACT_REQ_SEQ.NEXTVAL INTO :NEW.CONTACT_REQ_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</code>
  <enabled>true</enabled>
  <events>
    <event>INSERT</event>
  </events>
  <schema>
    <name>SCHEMA1</name>
  </schema>
  <source>CREATE TRIGGER CONTACT_REQ_TRG 
BEFORE INSERT ON CONTACT_REQ 
FOR EACH ROW 
BEGIN
  &lt;&lt;COLUMN_SEQUENCES&gt;&gt;
  BEGIN
    IF :NEW.CONTACT_REQ_ID IS NULL THEN
      SELECT CONTACT_REQ_SEQ.NEXTVAL INTO :NEW.CONTACT_REQ_ID FROM DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;</source>
  <statementLevel>false</statementLevel>
  <tableID class="oracle.javatools.db.IdentifierBasedID">
    <name>CONTACT_REQ</name>
    <identifier class="java.lang.String">290248f4-e0e6-4f24-8f0a-de3ce623391f</identifier>
    <schemaName>SCHEMA1</schemaName>
    <type>TABLE</type>
  </tableID>
  <timing>BEFORE</timing>
</trigger>
