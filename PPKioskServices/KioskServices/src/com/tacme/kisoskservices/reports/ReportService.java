package com.tacme.kisoskservices.reports;

import com.app.model.utils.PPServicesUtil;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;

import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import javax.servlet.http.HttpServletRequest;

public class ReportService {
    //    private static String realPath =
    //        "yourRelativePath/ConsumptionReport.jrxml";
    private static String LOGO = "/u01/app/UAQServices/reports/PPLogo.png";
    private static String CASE_JUDGEMENT_REPORT =
        "C://u01/app/MOJServices/reports/CaseJudgementReport.jasper";
    private static String CASE_DROPPING_REPORT =
        "C://u01/app/MOJServices/reports/CaseDroppingReport.jasper";
    private static String DECISION_OF_APPEAL_REPORT =
        "C://u01/app/MOJServices/reports/DecisionOfAppealReport.jasper";
    private static String PROVIDING_OBJECTION_REPORT =
        "C://u01/app/MOJServices/reports/ProvidingObjectionReport.jasper";
    private static String TO_WHOM_IT_MAY_CONC_REPORT =
        "C://u01/app/MOJServices/reports/ToWhomItMayConcReport.jasper";
    private static String NO_PAGES_MSG = "The document has no pages";

    public ReportService() {
        super();
    }

    public ByteArrayOutputStream generateCopyJudgeRep(String serviceId,
                                                              String requestId,
                                                              HttpServletRequest httpRequest) throws FileNotFoundException,
                                                                                                     JRException,
                                                                                                     NamingException,
                                                                                                     SQLException {
        Map<String, Object> params = new HashMap<String, Object>();
        ServletContext servletContext =
            httpRequest.getSession().getServletContext();
        //        System.err.println("servletContext ==>> " + servletContext);
        //        ServletContext servletContext =
        //            (ServletContext)httpRequest.getServletPath() .getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        //        realPath =
        //                servletContext.getRealPath("/jasperreports/LandEstimationReport.jasper");
        //        System.err.println("realPath ==>> " + realPath);
        //                params.put("pSitePlanId", sitePlanPOJO.getSitePlanId());

        //                params.put("pSitePlanMapImage", sitePlanPOJO.getFirstImage());
        //                params.put("pSitePlanImage", sitePlanPOJO.getSecondImage());
        params.put("IMG_PATH", LOGO);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(CASE_JUDGEMENT_REPORT);
        } catch (FileNotFoundException e) {
            throw e;
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        JasperReport report = null;
        try {
            report = (JasperReport)JRLoader.loadObject(fis);
        } catch (JRException e) {
            throw e;
        }

        Connection conn;
        try {
            conn = PPServicesUtil.getConnection();
        } catch (NamingException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        }
        try {
            JasperPrint jasperPrint =
                JasperFillManager.fillReport(report, params, conn);
            JRPdfExporter pdfPage = new JRPdfExporter();
            pdfPage.setParameter(JRExporterParameter.JASPER_PRINT,
                                 jasperPrint);
            pdfPage.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
            pdfPage.exportReport();

            if (jasperPrint.getPages().size() == 0)
                throw new JRException(NO_PAGES_MSG);
        } finally {
            conn.close();
        }
        return os;

    }

    public ByteArrayOutputStream generateCaseDroppingRep(String serviceId,
                                                              String requestId,
                                                              HttpServletRequest httpRequest) throws FileNotFoundException,
                                                                                                     JRException,
                                                                                                     NamingException,
                                                                                                     SQLException {
        Map<String, Object> params = new HashMap<String, Object>();
        ServletContext servletContext =
            httpRequest.getSession().getServletContext();
        //        System.err.println("servletContext ==>> " + servletContext);
        //        ServletContext servletContext =
        //            (ServletContext)httpRequest.getServletPath() .getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        //        realPath =
        //                servletContext.getRealPath("/jasperreports/LandEstimationReport.jasper");
        //        System.err.println("realPath ==>> " + realPath);
        //                params.put("pSitePlanId", sitePlanPOJO.getSitePlanId());

        //                params.put("pSitePlanMapImage", sitePlanPOJO.getFirstImage());
        //                params.put("pSitePlanImage", sitePlanPOJO.getSecondImage());
        params.put("IMG_PATH", LOGO);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(CASE_DROPPING_REPORT);
        } catch (FileNotFoundException e) {
            throw e;
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        JasperReport report = null;
        try {
            report = (JasperReport)JRLoader.loadObject(fis);
        } catch (JRException e) {
            throw e;
        }

        Connection conn;
        try {
            conn = PPServicesUtil.getConnection();
        } catch (NamingException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        }
        try {
            JasperPrint jasperPrint =
                JasperFillManager.fillReport(report, params, conn);
            JRPdfExporter pdfPage = new JRPdfExporter();
            pdfPage.setParameter(JRExporterParameter.JASPER_PRINT,
                                 jasperPrint);
            pdfPage.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
            pdfPage.exportReport();

            if (jasperPrint.getPages().size() == 0)
                throw new JRException(NO_PAGES_MSG);
        } finally {
            conn.close();
        }
        return os;

    }


    public ByteArrayOutputStream generateDecisionOfAppealRep(String serviceId,
                                                              String requestId,
                                                              HttpServletRequest httpRequest) throws FileNotFoundException,
                                                                                                     JRException,
                                                                                                     NamingException,
                                                                                                     SQLException {
        Map<String, Object> params = new HashMap<String, Object>();
        ServletContext servletContext =
            httpRequest.getSession().getServletContext();
        //        System.err.println("servletContext ==>> " + servletContext);
        //        ServletContext servletContext =
        //            (ServletContext)httpRequest.getServletPath() .getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        //        realPath =
        //                servletContext.getRealPath("/jasperreports/LandEstimationReport.jasper");
        //        System.err.println("realPath ==>> " + realPath);
        //                params.put("pSitePlanId", sitePlanPOJO.getSitePlanId());

        //                params.put("pSitePlanMapImage", sitePlanPOJO.getFirstImage());
        //                params.put("pSitePlanImage", sitePlanPOJO.getSecondImage());
        params.put("IMG_PATH", LOGO);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(DECISION_OF_APPEAL_REPORT);
        } catch (FileNotFoundException e) {
            throw e;
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        JasperReport report = null;
        try {
            report = (JasperReport)JRLoader.loadObject(fis);
        } catch (JRException e) {
            throw e;
        }

        Connection conn;
        try {
            conn = PPServicesUtil.getConnection();
        } catch (NamingException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        }
        try {
            JasperPrint jasperPrint =
                JasperFillManager.fillReport(report, params, conn);
            JRPdfExporter pdfPage = new JRPdfExporter();
            pdfPage.setParameter(JRExporterParameter.JASPER_PRINT,
                                 jasperPrint);
            pdfPage.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
            pdfPage.exportReport();

            if (jasperPrint.getPages().size() == 0)
                throw new JRException(NO_PAGES_MSG);
        } finally {
            conn.close();
        }
        return os;

    }


    public ByteArrayOutputStream generateProvidingObjRep(String serviceId,
                                                              String requestId,
                                                              HttpServletRequest httpRequest) throws FileNotFoundException,
                                                                                                     JRException,
                                                                                                     NamingException,
                                                                                                     SQLException {
        Map<String, Object> params = new HashMap<String, Object>();
        ServletContext servletContext =
            httpRequest.getSession().getServletContext();
        //        System.err.println("servletContext ==>> " + servletContext);
        //        ServletContext servletContext =
        //            (ServletContext)httpRequest.getServletPath() .getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        //        realPath =
        //                servletContext.getRealPath("/jasperreports/LandEstimationReport.jasper");
        //        System.err.println("realPath ==>> " + realPath);
        //                params.put("pSitePlanId", sitePlanPOJO.getSitePlanId());

        //                params.put("pSitePlanMapImage", sitePlanPOJO.getFirstImage());
        //                params.put("pSitePlanImage", sitePlanPOJO.getSecondImage());
        params.put("IMG_PATH", LOGO);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(PROVIDING_OBJECTION_REPORT);
        } catch (FileNotFoundException e) {
            throw e;
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        JasperReport report = null;
        try {
            report = (JasperReport)JRLoader.loadObject(fis);
        } catch (JRException e) {
            throw e;
        }

        Connection conn;
        try {
            conn = PPServicesUtil.getConnection();
        } catch (NamingException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        }
        try {
            JasperPrint jasperPrint =
                JasperFillManager.fillReport(report, params, conn);
            JRPdfExporter pdfPage = new JRPdfExporter();
            pdfPage.setParameter(JRExporterParameter.JASPER_PRINT,
                                 jasperPrint);
            pdfPage.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
            pdfPage.exportReport();

            if (jasperPrint.getPages().size() == 0)
                throw new JRException(NO_PAGES_MSG);
        } finally {
            conn.close();
        }
        return os;

    }


    public ByteArrayOutputStream generateToWhomItMayConcernRep(String serviceId,
                                                              String requestId,
                                                              HttpServletRequest httpRequest) throws FileNotFoundException,
                                                                                                     JRException,
                                                                                                     NamingException,
                                                                                                     SQLException {
        Map<String, Object> params = new HashMap<String, Object>();
        ServletContext servletContext =
            httpRequest.getSession().getServletContext();
        //        System.err.println("servletContext ==>> " + servletContext);
        //        ServletContext servletContext =
        //            (ServletContext)httpRequest.getServletPath() .getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        //        realPath =
        //                servletContext.getRealPath("/jasperreports/LandEstimationReport.jasper");
        //        System.err.println("realPath ==>> " + realPath);
        //                params.put("pSitePlanId", sitePlanPOJO.getSitePlanId());

        //                params.put("pSitePlanMapImage", sitePlanPOJO.getFirstImage());
        //                params.put("pSitePlanImage", sitePlanPOJO.getSecondImage());
        params.put("IMG_PATH", LOGO);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(TO_WHOM_IT_MAY_CONC_REPORT);
        } catch (FileNotFoundException e) {
            throw e;
        }
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        JasperReport report = null;
        try {
            report = (JasperReport)JRLoader.loadObject(fis);
        } catch (JRException e) {
            throw e;
        }

        Connection conn;
        try {
            conn = PPServicesUtil.getConnection();
        } catch (NamingException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        }
        try {
            JasperPrint jasperPrint =
                JasperFillManager.fillReport(report, params, conn);
            JRPdfExporter pdfPage = new JRPdfExporter();
            pdfPage.setParameter(JRExporterParameter.JASPER_PRINT,
                                 jasperPrint);
            pdfPage.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
            pdfPage.exportReport();

            if (jasperPrint.getPages().size() == 0)
                throw new JRException(NO_PAGES_MSG);
        } finally {
            conn.close();
        }
        return os;

    }


}
