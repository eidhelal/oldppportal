package com.tacme.kisoskservices.ifcs;

public interface ConstantsIfc {
    public static final String RESPONSE_SUCCESS_STATUS = "SUCCESS";
    public static final String VALIDATION_SUCCEEDED = "SUCCESS";
    public static final String RESPONSE_FAILURE_STATUS = "FAILURE";
    public static final int RESPONSE_DEFAULT_STATUS_CODE = -1;
    public static final String NO_DATA = "NO_DATA";
    public static final String SESSION_EXPIRED = "SESSION_EXPIRED";
    public static final String DATE_OF_BIRTH_MISMATCH = "DATE_OF_BIRTH_MISMATCH";
    public static final String USER_NOT_FOUND = "USER_NOT_FOUND";
    public static final String YES = "Y";
    public static final String NO = "N";
    public static final String ERROR = "ERROR";
}
