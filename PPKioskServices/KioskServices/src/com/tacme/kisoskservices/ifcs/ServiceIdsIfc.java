package com.tacme.kisoskservices.ifcs;

public class ServiceIdsIfc {
    public static final String DECISION_OF_APPEAL_AR = "26";
    public static final String DECISION_OF_APPEAL_EN = "1026";
    public static final String PROVIDING_OBJECTION_AR = "31";
    public static final String PROVIDING_OBJECTION_EN = "1031";
    
    public ServiceIdsIfc() {
        super();
    }
}
