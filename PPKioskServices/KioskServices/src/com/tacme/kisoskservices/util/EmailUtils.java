package com.tacme.kisoskservices.util;


import com.sun.mail.smtp.SMTPMessage;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import com.tacme.kisoskservices.pojos.EmailConfigPojo;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.UnsupportedEncodingException;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import javax.mail.internet.MimeMultipart;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.DataSource;

import javax.activation.MimeType;

import javax.activation.MimeTypeParseException;

import javax.imageio.ImageIO;

import javax.mail.internet.MimeUtility;
import javax.mail.internet.PreencodedMimeBodyPart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.codec.CharEncoding;

import weblogic.application.utils.IOUtils;

import weblogic.wsee.databinding.internal.runtime.MultipartMessageHelper;

public class EmailUtils {
    public EmailUtils() {
    }

    //    static private String _host;
    //    static private String _user;
    //    static private String _pwrd;
    //    static private String _senduser;
    //    static private String _port;

    //    public static void executeEmailView() {
    //        ViewObject emailView =
    //            ADFUtils.getApplicationModuleForDataControl("PPAppServicesDataControl").findViewObject("EmailConfigView1");
    //        emailView.setNamedWhereClauseParam("ecid", "PP1");
    //        emailView.executeQuery();
    //        Row emailRow = emailView.first();
    //       _host=((String)emailRow.getAttribute("EcHost"));
    //       _pwrd=((String)emailRow.getAttribute("EcUser"));
    //       _user=((String)emailRow.getAttribute("EcPwrd"));
    //        _senduser=((String)emailRow.getAttribute("EcSndMail"));
    //        _port=((String)emailRow.getAttribute("EcPort"));
    //    }

    public static void sendEmail(String emailAddress, String subject,
                                 String emailBody,
                                 EmailConfigPojo emailConfigPojo,
                                 String base64String,
                                 String requestId, String serviceName) throws MessagingException,
                                                         IOException {
        String[] toAdressList = null;
        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", "false");
        props.put("mail.smtp.host", emailConfigPojo.getHost());
        props.put("mail.smtp.user", emailConfigPojo.getUserName());
        props.put("mail.smtp.password", emailConfigPojo.getPassword());
        props.put("mail.smtp.port", emailConfigPojo.getPortNumber());
        props.put("mail.smtp.auth", "true");

        if (emailAddress.contains(",")) {
            toAdressList = emailAddress.split(",");
        } else {
            toAdressList = new String[1];
            toAdressList[0] = emailAddress;
        }
        Session session = Session.getDefaultInstance(props, null);
        MimeMessage message = new MimeMessage(session);
        InternetAddress[] toAddress = new InternetAddress[toAdressList.length];
        Multipart multipart = new MimeMultipart();
        BodyPart messageBodyPart = new MimeBodyPart();
        
        if(base64String != null){
        MimeBodyPart attachBodyPart = new MimeBodyPart();
            byte[] attachBytes = Base64.decode(base64String);
            DataSource ds =
                new ByteArrayDataSource(attachBytes, "application/pdf");
            attachBodyPart.setHeader("Content-Transfer-Encoding",
                                     "application/pdf");
            attachBodyPart.setDataHandler(new DataHandler(ds));
            attachBodyPart.setFileName(serviceName + "_" + requestId);
            multipart.addBodyPart(attachBodyPart);
        }

        messageBodyPart.setText(new String(emailBody.getBytes("UTF-8")));
        multipart.addBodyPart(messageBodyPart);
        
        message.setContent(multipart);
        try {
            // To get the array of addresses
            for (int i = 0; i < toAdressList.length; i++) {
                toAddress[i] = new InternetAddress(toAdressList[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
            message.setFrom(new InternetAddress(emailConfigPojo.getSendUser()));
            message.setSubject(subject, "UTF-8");
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        Transport transport = null;

        try {
            transport = session.getTransport("smtp");
            transport.connect(emailConfigPojo.getHost(),
                              emailConfigPojo.getUserName(),
                              emailConfigPojo.getPassword());
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }


    public static void sendHTMLEmail(String emailid, String subj, String txt,
                                     EmailConfigPojo emailConfigPojo) {

        //          executeEmailView();
        String[] toAdressList = null;
        Properties props = System.getProperties();
        props.put("mail.smtp.starttls.enable", "false");
        props.put("mail.smtp.host", emailConfigPojo.getHost());
        props.put("mail.smtp.user", emailConfigPojo.getUserName());
        props.put("mail.smtp.password", emailConfigPojo.getPassword());
        props.put("mail.smtp.port", emailConfigPojo.getPortNumber());
        props.put("mail.smtp.auth", "true");

        String toAddresses = emailid;
        if (toAddresses.contains(",")) {
            toAdressList = toAddresses.split(",");
        } else {
            toAdressList = new String[1];
            toAdressList[0] = toAddresses;
        }
        Session session = Session.getDefaultInstance(props, null);
        MimeMessage message = new MimeMessage(session);
        InternetAddress[] toAddress = new InternetAddress[toAdressList.length];
        BodyPart messageBodyPart = new MimeBodyPart();
        try {
            messageBodyPart.setText("This is message body");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        try {
            // To get the array of addresses
            for (int i = 0; i < toAdressList.length; i++) {
                toAddress[i] = new InternetAddress(toAdressList[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }
            message.setFrom(new InternetAddress(emailConfigPojo.getSendUser()));
            message.setSubject(subj, "UTF-8");
            message.setContent(txt, "text/html; charset=utf-8");
            //message.setText(txt, "text/html");
            //                    message.setSubject(subj);
            //
            //                    message.setText("<html><header></header><body></body></html>", "text/html");
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        Transport transport = null;

        try {
            transport = session.getTransport("smtp");
            transport.connect(emailConfigPojo.getHost(),
                              emailConfigPojo.getUserName(),
                              emailConfigPojo.getPassword());
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
