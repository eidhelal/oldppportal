package com.tacme.kisoskservices.util;

import com.app.model.services.PPKisokAppServiceImpl;

import com.app.model.utils.PPServicesUtil;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import oracle.jbo.domain.Timestamp;

import sun.misc.BASE64Encoder;

public class ServiceUtil {
    public ServiceUtil() {
        super();
    }

    public static PPKisokAppServiceImpl getKioskServiceAppModule() throws Exception {
        try {
            return PPServicesUtil.getCommonService();
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    /**
     *
     * @param date
     * @return
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar(java.util.Date date) {
        if (date == null) {
            return null;
        } else {
            DatatypeFactory df = null;
            try {
                df = DatatypeFactory.newInstance();
            } catch (DatatypeConfigurationException e) {
                throw new IllegalStateException("Error while trying to obtain a new instance of DatatypeFactory",
                                                e);
            }
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
            return df.newXMLGregorianCalendar(gc);
        }
    }

    // Converts an XMLGregorianCalendar to an instance of java.util.Date

    /**
     *
     * @param xmlGC
     * @return
     */
    public static java.util.Date asDate(XMLGregorianCalendar xmlGC) {
        if (xmlGC == null) {
            return null;
        } else {
            return xmlGC.toGregorianCalendar().getTime();
        }
    }

    /**
     *
     * @param xmlGC
     * @return
     */
    public static oracle.jbo.domain.Date asJboDate(XMLGregorianCalendar xmlGC) {
        java.util.Date javaDate = xmlGC.toGregorianCalendar().getTime();
        long longDate = javaDate.getTime();
        java.sql.Timestamp sqlDate = new java.sql.Timestamp(longDate);
        oracle.jbo.domain.Date jboDate = new oracle.jbo.domain.Date(sqlDate);
        return jboDate;
    }

    /**
     *
     * @param javaDate
     * @return
     */
    public static oracle.jbo.domain.Date asJboDate(java.util.Date javaDate) {
        long longDate = javaDate.getTime();
        java.sql.Timestamp sqlDate = new java.sql.Timestamp(longDate);
        oracle.jbo.domain.Date jboDate = new oracle.jbo.domain.Date(sqlDate);
        return jboDate;
    }

    /**
     * @param valueDate
     * @param days
     * @return
     */
    public static oracle.jbo.domain.Date addDaysToJboDate(oracle.jbo.domain.Date valueDate,
                                                          int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new java.util.Date(valueDate.timestampValue().getTime()));
        cal.add(Calendar.DATE, days);
        cal.clear(Calendar.HOUR);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        //        System.out.println("cal.getTimeInMillis()" + cal.getTimeInMillis());
        return new oracle.jbo.domain.Date(new java.sql.Timestamp(cal.getTimeInMillis()));

    }

    /**
     *
     * @param valueDate
     * @param month
     * @return
     */
    public static oracle.jbo.domain.Date addMonthesToJboDate(oracle.jbo.domain.Date valueDate,
                                                             int month) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new java.util.Date(valueDate.timestampValue().getTime()));
        cal.add(Calendar.MONTH, month);
        cal.clear(Calendar.HOUR);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        //        System.out.println("cal.getTimeInMillis()" + cal.getTimeInMillis());
        return new oracle.jbo.domain.Date(new java.sql.Timestamp(cal.getTimeInMillis()));

    }

    /**
     *
     * @param javaDate
     * @return
     */
    public static oracle.jbo.domain.Date asJboDate(Timestamp javaDate) {
        long longDate = javaDate.getTime();
        java.sql.Timestamp sqlDate = new java.sql.Timestamp(longDate);
        oracle.jbo.domain.Date jboDate = new oracle.jbo.domain.Date(sqlDate);
        return jboDate;
    }

    /**
     *
     * @param date
     * @return
     * @throws DatatypeConfigurationException
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar(oracle.jbo.domain.Timestamp date) throws DatatypeConfigurationException {
        if (date == null) {
            return null;
        } else {
            GregorianCalendar cal = new GregorianCalendar();

            cal.setTime(date.getValue());
            XMLGregorianCalendar xmlDate =
                DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);


            return xmlDate;
        }
    }

    /**
     *
     * @param date
     * @return
     * @throws DatatypeConfigurationException
     */
    public static XMLGregorianCalendar asXMLGregorianCalendar(oracle.jbo.domain.Date date) throws DatatypeConfigurationException {
        if (date == null) {
            return null;
        } else {
            GregorianCalendar cal = new GregorianCalendar();

            cal.setTime(date.getValue());
            XMLGregorianCalendar xmlDate =
                DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);


            return xmlDate;
        }
    }

    /**
     * This method is used to check if string is null or empty
     * @param obj
     * @return
     */
    public static boolean isNullOrEmpty(String obj) {
        if (obj == null || obj.length() == 0)
            return true;
        return false;
    }

    /**
     * This method used for generating a global unique Ids
     * @return String(Generated Unique Id)
     */
    public static String getGUID() {
        return UUID.randomUUID().toString();
    }

    public static String getOTP() {

        StringBuilder generatedToken = new StringBuilder();
        try {
            SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
            // Generate 20 integers 0..20
            for (int i = 0; i < 6; i++) {
                generatedToken.append(number.nextInt(9));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return generatedToken.toString();
    }

    public static String hashPassword(String password) throws Exception {
        SecretKey key = null;
        String publicKey_var =
            "sdjkhfsdhfksafdsfdlskhflksdjhflsdhflhsdlkfhlskdf";
        String encrypedPwd = null;


        DESKeySpec keySpec = new DESKeySpec(publicKey_var.getBytes());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        key = keyFactory.generateSecret(keySpec);


        BASE64Encoder base64encoder = new BASE64Encoder();
        byte[] cleartext = password.getBytes("UTF8");
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        encrypedPwd = base64encoder.encode(cipher.doFinal(cleartext));


        return encrypedPwd;
    }

    public static Double convertAmountISOToDecimalFormat(String isoAmount) {
        if (isNullOrEmpty(isoAmount))
            return Double.valueOf(0.0D);

        return Double.valueOf(Double.valueOf(isoAmount).doubleValue() /
                              100.0D);
    }

    public static java.util.Date asUtilDate(oracle.jbo.domain.Date domainDate) {
        java.util.Date date = null;
        if (domainDate != null) {
            java.sql.Date sqldate = domainDate.dateValue();
            date = new Date(sqldate.getTime());
        }
        return date;
    }
    
    public static java.util.Date asUtilDate(String dateStr) throws ParseException {
//        java.util.Date date = new SimpleDateFormat("dd-MM-yyyy").format(dateStr);
        System.err.println("dateStr ==>> " + dateStr);
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = formatter.parse(dateStr);
        System.out.println("date ==>> " + date);

        return date;
    }


    public static oracle.jbo.domain.Number getDiffBetweenTwoDate(oracle.jbo.domain.Date startDate,
                                                                 oracle.jbo.domain.Date endDate) {
        float nYears = 0;
        int miliSecondPerDay = 86400000;
        if (startDate != null && endDate != null) {
            java.sql.Timestamp ts1 = startDate.timestampValue();
            java.sql.Timestamp ts2 = endDate.timestampValue();
            long ndays = (ts2.getTime() - ts1.getTime()) / miliSecondPerDay;
            nYears = ndays / 365;
        }
        return new oracle.jbo.domain.Number(nYears);

    }


    public static oracle.jbo.domain.Date getCurrentOracleDateTime() {
        return new oracle.jbo.domain.Date(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    public static String formatDate(Date date) {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(date);
    }

    public static String encodedUser() {

        return new BASE64Encoder().encode(("uaqdev" + ":" +
                                           "welcome1").getBytes());
    }
    
    public static oracle.jbo.domain.Date asJboDate(String inputDate) {
        oracle.jbo.domain.Date jboDate = null;
        if (inputDate != null) {
            try {
                java.util.Date date;
                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                date = formatter.parse(inputDate);
                java.sql.Date sqlDate = new java.sql.Date(date.getTime());
                jboDate = new oracle.jbo.domain.Date(sqlDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return jboDate;
    }

}
