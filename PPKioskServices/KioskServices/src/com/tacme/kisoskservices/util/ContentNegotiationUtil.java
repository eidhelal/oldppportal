package com.tacme.kisoskservices.util;

import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;

public class ContentNegotiationUtil {
    public ContentNegotiationUtil() {
        super();
    }
    /**
     * Used to get best response variant for selected request
     * @param restRequest
     * @return Variant
     *         best varaint for current request
     * @throws OdzMobileException
     */
    public static Variant getResponseVariant(Request restRequest) throws Exception {
        Variant bestResponseVariant = null;
        try {
            List<Variant> responseVariants =
                Variant.mediaTypes(MediaType.valueOf(MediaType.APPLICATION_JSON),
                                   MediaType.valueOf(MediaType.APPLICATION_XML +
                                                     ";charset=UTF-8"),
                                   MediaType.valueOf(MediaType.APPLICATION_XML +
                                                     ";charset=shift_jis"),
                                   MediaType.valueOf(MediaType.APPLICATION_FORM_URLENCODED)).encodings("gzip",
                                                                                                       "identity",
                                                                                                       "deflate").add().build();
            bestResponseVariant = restRequest.selectVariant(responseVariants);
        } catch (Exception ex) {
            throw new Exception(ex);        
            }
        return bestResponseVariant;
    }

    /**
     * Used to get response status code
     * @param bestResponseVariant
     * @return int
     *          Response HTTP code either 400 , 403 , 404 , 500 , 200
     * @throws OdzMobileException
     */
    public static int getStatusCode(Variant bestResponseVariant) throws Exception {
        int statusCode = Response.Status.OK.getStatusCode();

        try {
            if (bestResponseVariant == null) {
                /* Based on results,
                          the optimal response variant can not be determined from the list given.
                       */
                statusCode = Response.Status.NOT_ACCEPTABLE.getStatusCode();
            } else {
                MediaType responseMediaType =
                    bestResponseVariant.getMediaType();

                if (responseMediaType.isCompatible(MediaType.APPLICATION_XML_TYPE)) {
                    //entity in XML format
                    statusCode = Response.Status.OK.getStatusCode();
                } else if (responseMediaType.isCompatible(MediaType.APPLICATION_JSON_TYPE)) {
                    //entity in JSON format
                    statusCode = Response.Status.OK.getStatusCode();
                } else if (responseMediaType.isCompatible(MediaType.APPLICATION_FORM_URLENCODED_TYPE)) {
                    //entity in JSON format
                    statusCode = Response.Status.OK.getStatusCode();
                } else {
                    statusCode =
                            Response.Status.NOT_ACCEPTABLE.getStatusCode();
                }

            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return statusCode;
    }

    /**
     * Used to get media type selected by caller
     * @param bestResponseVariant
     * @return String
     *          Selected media type
     * @throws OdzMobileException
     */
    public static String getMediaType(Variant bestResponseVariant) throws Exception {
        String mediaType = MediaType.APPLICATION_JSON+ ";charset=utf-8";
        bestResponseVariant = null ;
        return mediaType;
    }
}
