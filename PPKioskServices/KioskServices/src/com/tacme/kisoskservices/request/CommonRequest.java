package com.tacme.kisoskservices.request;


import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@XmlRootElement

public class CommonRequest {
    private String serviceId;
    private String prosId;
    private String caseNumber;
    private String caseYear;
    private String caseType;
    private String relationCase;
    private String locale;
    private String selectedSession;
    private String userId;
    private String appRef;
    private String certificateTypeId;
    private String concernedParty;
    private String isSms;
    private String isEmail;
    private String memoSubject;
    private String complainantMobile;
    private String complainantName;
    private String complainantNationalityId;
    private String complainantAddress;
    private String memoCategory;
    private String accusedName;
    private String requestedByName;
    private String inputStream;
    private String fileName;
    private String transactionStatus;
    private String requestId;
    private String statusDetails;
    private String emiratesId;
    private String contentType;

    public CommonRequest() {
        super();
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setProsId(String prosId) {
        this.prosId = prosId;
    }

    public String getProsId() {
        return prosId;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseYear(String caseYear) {
        this.caseYear = caseYear;
    }

    public String getCaseYear() {
        return caseYear;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setRelationCase(String relationCase) {
        this.relationCase = relationCase;
    }

    public String getRelationCase() {
        return relationCase;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setSelectedSession(String selectedSession) {
        this.selectedSession = selectedSession;
    }

    public String getSelectedSession() {
        return selectedSession;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setAppRef(String appRef) {
        this.appRef = appRef;
    }

    public String getAppRef() {
        return appRef;
    }

    public void setCertificateTypeId(String certificateTypeId) {
        this.certificateTypeId = certificateTypeId;
    }

    public String getCertificateTypeId() {
        return certificateTypeId;
    }

    public void setConcernedParty(String concernedParty) {
        this.concernedParty = concernedParty;
    }

    public String getConcernedParty() {
        return concernedParty;
    }

    public void setIsSms(String isSms) {
        this.isSms = isSms;
    }

    public String getIsSms() {
        return isSms;
    }

    public void setIsEmail(String isEmail) {
        this.isEmail = isEmail;
    }

    public String getIsEmail() {
        return isEmail;
    }

    public void setMemoSubject(String memoSubject) {
        this.memoSubject = memoSubject;
    }

    public String getMemoSubject() {
        return memoSubject;
    }

    public void setComplainantMobile(String complainantMobile) {
        this.complainantMobile = complainantMobile;
    }

    public String getComplainantMobile() {
        return complainantMobile;
    }

    public void setComplainantName(String complainantName) {
        this.complainantName = complainantName;
    }

    public String getComplainantName() {
        return complainantName;
    }

    public void setComplainantNationalityId(String complainantNationalityId) {
        this.complainantNationalityId = complainantNationalityId;
    }

    public String getComplainantNationalityId() {
        return complainantNationalityId;
    }

    public void setComplainantAddress(String complainantAddress) {
        this.complainantAddress = complainantAddress;
    }

    public String getComplainantAddress() {
        return complainantAddress;
    }

    public void setMemoCategory(String memoCategory) {
        this.memoCategory = memoCategory;
    }

    public String getMemoCategory() {
        return memoCategory;
    }

    public void setAccusedName(String accusedName) {
        this.accusedName = accusedName;
    }

    public String getAccusedName() {
        return accusedName;
    }

    public void setRequestedByName(String requestedByName) {
        this.requestedByName = requestedByName;
    }

    public String getRequestedByName() {
        return requestedByName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    public String getStatusDetails() {
        return statusDetails;
    }

    public void setEmiratesId(String emiratesId) {
        this.emiratesId = emiratesId;
    }

    public String getEmiratesId() {
        return emiratesId;
    }

    public void setInputStream(String inputStream) {
        this.inputStream = inputStream;
    }

    public String getInputStream() {
        return inputStream;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }
}
