package com.tacme.kisoskservices.webservice;

import com.tacme.kisoskservices.ifcs.ConstantsIfc;
import com.tacme.kisoskservices.pojos.ServiceDetailsPojo;
import com.tacme.kisoskservices.pojos.UserInfoPojo;
import com.tacme.kisoskservices.response.CommonResponse;
import com.tacme.kisoskservices.response.ServiceDetailsResponse;
import com.tacme.kisoskservices.response.UserInfoResponse;
import com.tacme.kisoskservices.service.LoginService;

import com.tacme.kisoskservices.util.ContentNegotiationUtil;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;

@Path("json/login")
@Produces(MediaType.APPLICATION_JSON)

public class LoginWsImpl {
    public LoginWsImpl() {
        super();
    }
    
    @GET
    @Path("/login/{userName}/{password}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("userName")
        String userName, @PathParam("password")
        String password, @PathParam("dateOfBirth") 
        String dateOfBirth) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        UserInfoPojo result = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        UserInfoResponse userInfoResponse = new UserInfoResponse();
        LoginService commonService = new LoginService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            result = commonService.login(userName, password, dateOfBirth);
//                userInfoResponse.setMessage(result.getMessage());
            if (result != null && result.getUserId() != null) {
//                String guid = generateGuid();
                userInfoResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
            userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                userInfoResponse.setUserInfo(result);
//                serviceResponse.setGuid(guid);
//                commonService.createSession(userName, guid);
            } else{
                userInfoResponse.setMessage(ConstantsIfc.USER_NOT_FOUND);
                userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            userInfoResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(userInfoResponse).build();
    }
    
    @GET
    @Path("/login/{emiratesId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("emiratesId")
        String emiratesId, @PathParam("dateOfBirth") String dateOfBirth) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        UserInfoPojo result = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        UserInfoResponse userInfoResponse = new UserInfoResponse();
        LoginService commonService = new LoginService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            result = commonService.login(emiratesId, dateOfBirth);
//                userInfoResponse.setMessage(result.getMessage());
            if (result != null && result.getUserId() != null) {
//                String guid = generateGuid();
                 userInfoResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
             userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                userInfoResponse.setUserInfo(result);
//                serviceResponse.setGuid(guid);
//                commonService.createSession(userName, guid);
            } else{
                userInfoResponse.setMessage(ConstantsIfc.USER_NOT_FOUND);
                userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            userInfoResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(userInfoResponse).build();
    }
}
