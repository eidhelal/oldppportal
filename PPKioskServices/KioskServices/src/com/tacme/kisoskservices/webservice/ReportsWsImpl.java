package com.tacme.kisoskservices.webservice;


import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import com.tacme.kisoskservices.ifcs.ConstantsIfc;
import com.tacme.kisoskservices.pojos.RidcPojo;
import com.tacme.kisoskservices.pojos.UserInfoPojo;
import com.tacme.kisoskservices.reports.ReportService;
import com.tacme.kisoskservices.response.ReportsResponse;
import com.tacme.kisoskservices.response.UserInfoResponse;
import com.tacme.kisoskservices.service.LoginService;
import com.tacme.kisoskservices.service.RIDCService;
import com.tacme.kisoskservices.util.ContentNegotiationUtil;

import java.io.ByteArrayOutputStream;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;

@Path("json/common/reports")
@Produces(MediaType.APPLICATION_JSON)
public class ReportsWsImpl {
    public ReportsWsImpl() {
        super();
    }
 
    @GET
    @Path("/generateCopyJudgeRep/{serviceId}/{requestId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateCopyJudgeRep(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("serviceId")
        String serviceId, @PathParam("requestId")
        String requestId) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
            ReportsResponse reportsResponse = new ReportsResponse();
        try {
        ReportService reportService = new ReportService();
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ByteArrayOutputStream os =
                reportService.generateCopyJudgeRep(serviceId,
                                                           requestId,
                                                           httpRequest);
            if (os != null) {
                reportsResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setReportResult(encodeToBase64(os));
                RIDCService ridcService = new RIDCService();
                ridcService.createFolder(serviceId);
//                RidcPojo ridcPojo = ridcService.uploadedFile(requestId, reportsResponse.getReportResult(),
//                                                           requestId,serviceId);
//                System.err.println("attachUrl ==>> " + ridcPojo.getAttachDownloadUrl() + " and did ===>> " + ridcPojo.getAttachDid());
            } else {
            reportsResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            //            userInfoResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            //            userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(reportsResponse).build();
    }
        
    @GET
    @Path("/generateCaseDroppingRep/{serviceId}/{requestId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateCaseDroppingRep(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("serviceId")
        String serviceId, @PathParam("requestId")
        String requestId) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
            ReportsResponse reportsResponse = new ReportsResponse();
        try {
        ReportService reportService = new ReportService();
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ByteArrayOutputStream os =
                reportService.generateCaseDroppingRep(serviceId,
                                                           requestId,
                                                           httpRequest);
            if (os != null) {
                reportsResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setReportResult(encodeToBase64(os));
            } else {
            reportsResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            //            userInfoResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            //            userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(reportsResponse).build();
    }
        
    @GET
    @Path("/generateDecisionOfAppealRep/{serviceId}/{requestId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateDecisionOfAppealRep(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("serviceId")
        String serviceId, @PathParam("requestId")
        String requestId) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
            ReportsResponse reportsResponse = new ReportsResponse();
        try {
        ReportService reportService = new ReportService();
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ByteArrayOutputStream os =
                reportService.generateDecisionOfAppealRep(serviceId,
                                                           requestId,
                                                           httpRequest);
            if (os != null) {
                reportsResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setReportResult(encodeToBase64(os));
            } else {
            reportsResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            //            userInfoResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            //            userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(reportsResponse).build();
    }
        
    @GET
    @Path("/generateToWhomItMayConcernRep/{serviceId}/{requestId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateToWhomItMayConcernRep(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("serviceId")
        String serviceId, @PathParam("requestId")
        String requestId) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
            ReportsResponse reportsResponse = new ReportsResponse();
        try {
        ReportService reportService = new ReportService();
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ByteArrayOutputStream os =
                reportService.generateToWhomItMayConcernRep(serviceId,
                                                           requestId,
                                                           httpRequest);
            if (os != null) {
                reportsResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setReportResult(encodeToBase64(os));
            } else {
            reportsResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            //            userInfoResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            //            userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(reportsResponse).build();
    }
        
    @GET
    @Path("/generateProvidingObjRep/{serviceId}/{requestId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateProvidingObjRep(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("serviceId")
        String serviceId, @PathParam("requestId")
        String requestId) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
            ReportsResponse reportsResponse = new ReportsResponse();
        try {
        ReportService reportService = new ReportService();
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ByteArrayOutputStream os =
                reportService.generateProvidingObjRep(serviceId,
                                                           requestId,
                                                           httpRequest);
            if (os != null) {
                reportsResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                reportsResponse.setReportResult(encodeToBase64(os));
            } else {
            reportsResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                reportsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            //            userInfoResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            //            userInfoResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(reportsResponse).build();
    }

    private String encodeToBase64(ByteArrayOutputStream os) {
        String base64String = Base64.encode(os.toByteArray());
        System.err.println("base64String ==>> " + base64String);
        return base64String;
    }
}
