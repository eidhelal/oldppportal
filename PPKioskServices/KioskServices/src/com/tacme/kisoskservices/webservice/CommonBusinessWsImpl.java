package com.tacme.kisoskservices.webservice;

import com.app.model.pojos.ResultPojo;

import com.tacme.kisoskservices.ifcs.ConstantsIfc;
import com.tacme.kisoskservices.pojos.CaseDetailsPojo;
import com.tacme.kisoskservices.pojos.EmailConfigPojo;
import com.tacme.kisoskservices.pojos.RequestDetailsPojo;
import com.tacme.kisoskservices.pojos.RidcPojo;
import com.tacme.kisoskservices.pojos.ServiceDetailsPojo;
import com.tacme.kisoskservices.pojos.UserInfoPojo;
import com.tacme.kisoskservices.request.CommonRequest;
import com.tacme.kisoskservices.response.CasesInquiryResponse;
import com.tacme.kisoskservices.response.CommonResponse;
import com.tacme.kisoskservices.response.RequestsDetailsResponse;
import com.tacme.kisoskservices.response.UserInfoResponse;
import com.tacme.kisoskservices.service.CommonBusinessService;
import com.tacme.kisoskservices.service.LookupService;
import com.tacme.kisoskservices.service.RIDCService;
import com.tacme.kisoskservices.util.ContentNegotiationUtil;

import com.tacme.kisoskservices.util.EmailUtils;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.activation.MimeTypeParseException;

import javax.mail.MessagingException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;


@Path("json/common/businessServices")
public class CommonBusinessWsImpl {
    public CommonBusinessWsImpl() {
        super();
    }

    @POST
    @Path("/submitCopyJudgeReq")
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitCopyJudgeReq(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, CommonRequest copyJudgeReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ResultPojo validateResultPojo = null;
            //                commonBusinessService.validateCase(openingOfMemorandumsReq.getServiceId(),
            //                                                    openingOfMemorandumsReq.getCaseNumber(),
            //                                                    openingOfMemorandumsReq.getCaseYear(),
            //                                                    openingOfMemorandumsReq.getProsId(),
            //                                                    openingOfMemorandumsReq.getEmiratesId(),
            //                                                    openingOfMemorandumsReq.getLocale(), openingOfMemorandumsReq.getCertificateTypeId());
            validateResultPojo = new ResultPojo();
            validateResultPojo.setStatus(ConstantsIfc.VALIDATION_SUCCEEDED);
            if (validateResultPojo != null) {
                if (validateResultPojo.getStatus() != null &&
                    validateResultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.VALIDATION_SUCCEEDED)) {
                    String requestId =
                        commonBusinessService.submitCopyJudgeReq(copyJudgeReq);
                    validateResultPojo.setServiceFees(10);
                    String serviceFees =
                        validateResultPojo.getServiceFees() != null ?
                        validateResultPojo.getServiceFees().toString() : "0";
                    commonResponse =
                            handleServiceResponse(commonResponse, commonBusinessService,
                                                  copyJudgeReq.getUserId(),
                                                  serviceFees, requestId, true,
                                                  copyJudgeReq.getServiceId(),
                                                  copyJudgeReq.getCaseNumber(),
                                                  copyJudgeReq.getCaseYear(),
                                                  copyJudgeReq.getProsId(),
                                                  null);
                    commonBusinessService.commitAndRelease();
                } else {
                    commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                    commonResponse.setMessage(validateResultPojo.getMessage());
                    // commonResponse.setCanContinue(ConstantsIfc.YES);
                }
            } else {
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.NO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @POST
    @Path("/submitCopyOfCaseDropping")
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitCopyOfCaseDropping(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, CommonRequest copyOfCaseDroppingReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ResultPojo validateResultPojo = null;
            //                commonBusinessService.validateCase(openingOfMemorandumsReq.getServiceId(),
            //                                                    openingOfMemorandumsReq.getCaseNumber(),
            //                                                    openingOfMemorandumsReq.getCaseYear(),
            //                                                    openingOfMemorandumsReq.getProsId(),
            //                                                    openingOfMemorandumsReq.getEmiratesId(),
            //                                                    openingOfMemorandumsReq.getLocale(), openingOfMemorandumsReq.getCertificateTypeId());
            validateResultPojo = new ResultPojo();
            validateResultPojo.setStatus(ConstantsIfc.VALIDATION_SUCCEEDED);
            if (validateResultPojo != null) {
                if (validateResultPojo.getStatus() != null &&
                    validateResultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.VALIDATION_SUCCEEDED)) {
                    String requestId =
                        commonBusinessService.submitCopyOfCaseDroppingReq(copyOfCaseDroppingReq);
                    validateResultPojo.setServiceFees(10.00);
                    String serviceFees =
                        validateResultPojo.getServiceFees() != null ?
                        validateResultPojo.getServiceFees().toString() : "0";
                    commonResponse =
                            handleServiceResponse(commonResponse, commonBusinessService,
                                                  copyOfCaseDroppingReq.getUserId(),
                                                  serviceFees, requestId, true,
                                                  copyOfCaseDroppingReq.getServiceId(),
                                                  copyOfCaseDroppingReq.getCaseNumber(),
                                                  copyOfCaseDroppingReq.getCaseYear(),
                                                  copyOfCaseDroppingReq.getProsId(),
                                                  null);
                    commonBusinessService.commitAndRelease();
                } else {
                    commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                    commonResponse.setMessage(validateResultPojo.getMessage());
                    // commonResponse.setCanContinue(true);
                }
            } else {
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            //            commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            // commonResponse.setCanContinue(ConstantsIfc.NO);
            commonResponse.setStatusCode(ConstantsIfc.ERROR);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @POST
    @Path("/submitToWhomItMayConcernReq")
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitToWhomItMayConcernReq(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, CommonRequest toWhomItMayConcernReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ResultPojo validateResultPojo = null;
            //                commonBusinessService.validateCase(openingOfMemorandumsReq.getServiceId(),
            //                                                    openingOfMemorandumsReq.getCaseNumber(),
            //                                                    openingOfMemorandumsReq.getCaseYear(),
            //                                                    openingOfMemorandumsReq.getProsId(),
            //                                                    openingOfMemorandumsReq.getEmiratesId(),
            //                                                    openingOfMemorandumsReq.getLocale(), openingOfMemorandumsReq.getCertificateTypeId());
            validateResultPojo = new ResultPojo();
            validateResultPojo.setStatus(ConstantsIfc.VALIDATION_SUCCEEDED);
            if (validateResultPojo != null) {
                if (validateResultPojo.getStatus() != null &&
                    validateResultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.VALIDATION_SUCCEEDED)) {
                    String requestId =
                        commonBusinessService.submitToWhomItMayConcernReq(toWhomItMayConcernReq);
                    validateResultPojo.setServiceFees(10);
                    String serviceFees =
                        validateResultPojo.getServiceFees() != null ?
                        validateResultPojo.getServiceFees().toString() : "0";
                    commonResponse =
                            handleServiceResponse(commonResponse, commonBusinessService,
                                                  toWhomItMayConcernReq.getUserId(),
                                                  serviceFees, requestId, true,
                                                  toWhomItMayConcernReq.getServiceId(),
                                                  toWhomItMayConcernReq.getCaseNumber(),
                                                  toWhomItMayConcernReq.getCaseYear(),
                                                  toWhomItMayConcernReq.getProsId(),
                                                  null);
                    commonBusinessService.commitAndRelease();
                } else {
                    commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                    commonResponse.setMessage(validateResultPojo.getMessage());
                    // commonResponse.setCanContinue(ConstantsIfc.YES);
                }
            } else {
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.NO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @POST
    @Path("/submitDecisionOfAppeal")
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitDecisionOfAppeal(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, CommonRequest decisionOfAppealReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ResultPojo validateResultPojo = null;
            //                commonBusinessService.validateCase(openingOfMemorandumsReq.getServiceId(),
            //                                                    openingOfMemorandumsReq.getCaseNumber(),
            //                                                    openingOfMemorandumsReq.getCaseYear(),
            //                                                    openingOfMemorandumsReq.getProsId(),
            //                                                    openingOfMemorandumsReq.getEmiratesId(),
            //                                                    openingOfMemorandumsReq.getLocale(), openingOfMemorandumsReq.getCertificateTypeId());
            validateResultPojo = new ResultPojo();
            validateResultPojo.setStatus(ConstantsIfc.VALIDATION_SUCCEEDED);
            if (validateResultPojo != null) {
                if (validateResultPojo.getStatus() != null &&
                    validateResultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.VALIDATION_SUCCEEDED)) {
                    String requestId =
                        commonBusinessService.submitDecisionOfAppealReq(decisionOfAppealReq);
                    validateResultPojo.setServiceFees(0);
                    String serviceFees =
                        validateResultPojo.getServiceFees() != null ?
                        validateResultPojo.getServiceFees().toString() : "0";
                    commonResponse =
                            handleServiceResponse(commonResponse, commonBusinessService,
                                                  decisionOfAppealReq.getUserId(),
                                                  serviceFees, requestId,
                                                  false,
                                                  decisionOfAppealReq.getServiceId(),
                                                  decisionOfAppealReq.getCaseNumber(),
                                                  decisionOfAppealReq.getCaseYear(),
                                                  decisionOfAppealReq.getProsId(),
                                                  null);
                    commonBusinessService.commitAndRelease();
                } else {
                    commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                    commonResponse.setMessage(validateResultPojo.getMessage());
                    // commonResponse.setCanContinue(ConstantsIfc.YES);
                }
            } else {
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.NO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @POST
    @Path("/subscribeToSmsServiceReq")
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscribeToSmsServiceReq(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest,
        CommonRequest subscribeToSmsServiceReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();

        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ResultPojo validateResultPojo = null;
            //                commonBusinessService.validateCase(openingOfMemorandumsReq.getServiceId(),
            //                                                    openingOfMemorandumsReq.getCaseNumber(),
            //                                                    openingOfMemorandumsReq.getCaseYear(),
            //                                                    openingOfMemorandumsReq.getProsId(),
            //                                                    openingOfMemorandumsReq.getEmiratesId(),
            //                                                    openingOfMemorandumsReq.getLocale(), openingOfMemorandumsReq.getCertificateTypeId());
            validateResultPojo = new ResultPojo();
            validateResultPojo.setStatus(ConstantsIfc.VALIDATION_SUCCEEDED);
            if (validateResultPojo != null) {
                if (validateResultPojo.getStatus() != null &&
                    validateResultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.VALIDATION_SUCCEEDED)) {
                    String requestId =
                        commonBusinessService.subscribeToSmsServiceReq(subscribeToSmsServiceReq);
                    validateResultPojo.setServiceFees(0);
                    String serviceFees =
                        validateResultPojo.getServiceFees() != null ?
                        validateResultPojo.getServiceFees().toString() : "0";
                    commonResponse =
                            handleServiceResponse(commonResponse, commonBusinessService,
                                                  subscribeToSmsServiceReq.getUserId(),
                                                  serviceFees, requestId,
                                                  false,
                                                  subscribeToSmsServiceReq.getServiceId(),
                                                  subscribeToSmsServiceReq.getCaseNumber(),
                                                  subscribeToSmsServiceReq.getCaseYear(),
                                                  subscribeToSmsServiceReq.getProsId(),
                                                  null);
                    commonBusinessService.commitAndRelease();
                } else {
                    commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                    commonResponse.setMessage(validateResultPojo.getMessage());
                    // commonResponse.setCanContinue(ConstantsIfc.YES);
                }
            } else {
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.NO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @POST
    @Path("/submitProvidingAnObjection")
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitProvidingAnObjection(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest,
        CommonRequest providingAnObjectionReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();

        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ResultPojo validateResultPojo = null;
            //                commonBusinessService.validateCase(openingOfMemorandumsReq.getServiceId(),
            //                                                    openingOfMemorandumsReq.getCaseNumber(),
            //                                                    openingOfMemorandumsReq.getCaseYear(),
            //                                                    openingOfMemorandumsReq.getProsId(),
            //                                                    openingOfMemorandumsReq.getEmiratesId(),
            //                                                    openingOfMemorandumsReq.getLocale(), openingOfMemorandumsReq.getCertificateTypeId());
            validateResultPojo = new ResultPojo();
            validateResultPojo.setStatus(ConstantsIfc.VALIDATION_SUCCEEDED);
            if (validateResultPojo != null) {
                if (validateResultPojo.getStatus() != null &&
                    validateResultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.VALIDATION_SUCCEEDED)) {
                    String requestId =
                        commonBusinessService.submitProvidingAnObjectionReq(providingAnObjectionReq);
                    validateResultPojo.setServiceFees(0);
                    String serviceFees =
                        validateResultPojo.getServiceFees() != null ?
                        validateResultPojo.getServiceFees().toString() : "0";
                    commonResponse =
                            handleServiceResponse(commonResponse, commonBusinessService,
                                                  providingAnObjectionReq.getUserId(),
                                                  serviceFees, requestId,
                                                  false,
                                                  providingAnObjectionReq.getServiceId(),
                                                  providingAnObjectionReq.getCaseNumber(),
                                                  providingAnObjectionReq.getCaseYear(),
                                                  providingAnObjectionReq.getProsId(),
                                                  null);
                    commonBusinessService.commitAndRelease();
                } else {
                    commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                    commonResponse.setMessage(validateResultPojo.getMessage());
                    // commonResponse.setCanContinue(ConstantsIfc.YES);
                }
            } else {
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.NO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @POST
    @Path("/submitPayFineReq")
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitPayFineReq(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, CommonRequest payFineReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();

        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ResultPojo validateResultPojo = null;
            //                commonBusinessService.validateCase(openingOfMemorandumsReq.getServiceId(),
            //                                                    openingOfMemorandumsReq.getCaseNumber(),
            //                                                    openingOfMemorandumsReq.getCaseYear(),
            //                                                    openingOfMemorandumsReq.getProsId(),
            //                                                    openingOfMemorandumsReq.getEmiratesId(),
            //                                                    openingOfMemorandumsReq.getLocale(), openingOfMemorandumsReq.getCertificateTypeId());
            validateResultPojo = new ResultPojo();
            validateResultPojo.setStatus(ConstantsIfc.VALIDATION_SUCCEEDED);
            if (validateResultPojo != null) {
                if (validateResultPojo.getStatus() != null &&
                    validateResultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.VALIDATION_SUCCEEDED)) {
                    String requestId =
                        commonBusinessService.submitPayFineReq(payFineReq);
                    validateResultPojo.setServiceFees(0);
                    String serviceFees =
                        validateResultPojo.getServiceFees() != null ?
                        validateResultPojo.getServiceFees().toString() : "0";
                    commonResponse =
                            handleServiceResponse(commonResponse, commonBusinessService,
                                                  payFineReq.getUserId(),
                                                  serviceFees, requestId,
                                                  false,
                                                  payFineReq.getServiceId(),
                                                  payFineReq.getCaseNumber(),
                                                  payFineReq.getCaseYear(),
                                                  payFineReq.getProsId(),
                                                  null);
                    commonBusinessService.commitAndRelease();
                } else {
                    commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                    commonResponse.setMessage(validateResultPojo.getMessage());
                    // commonResponse.setCanContinue(ConstantsIfc.YES);
                }
            } else {
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.NO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @POST
    @Path("/submitOpeningOfMemorandumsReq")
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitOpeningOfMemorandumsReq(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest,
        CommonRequest openingOfMemorandumsReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);

            ResultPojo validateResultPojo = null;
            //                commonBusinessService.validateCase(openingOfMemorandumsReq.getServiceId(),
            //                                                    openingOfMemorandumsReq.getCaseNumber(),
            //                                                    openingOfMemorandumsReq.getCaseYear(),
            //                                                    openingOfMemorandumsReq.getProsId(),
            //                                                    openingOfMemorandumsReq.getEmiratesId(),
            //                                                    openingOfMemorandumsReq.getLocale(), openingOfMemorandumsReq.getCertificateTypeId());
            validateResultPojo = new ResultPojo();
            validateResultPojo.setStatus(ConstantsIfc.VALIDATION_SUCCEEDED);
            if (validateResultPojo != null) {
                if (validateResultPojo.getStatus() != null &&
                    validateResultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.VALIDATION_SUCCEEDED)) {
                    String requestId =
                        commonBusinessService.submitOpeningOfMemorandumsReq(openingOfMemorandumsReq);
                    String fileName =
                        requestId + "_" + openingOfMemorandumsReq.getFileName();
                    RIDCService ridcService = new RIDCService();
                    RidcPojo ridcPojo =
                        ridcService.uploadedFile(fileName, openingOfMemorandumsReq.getInputStream(),
                                                 requestId);
                    validateResultPojo.setServiceFees(0);
                    String serviceFees =
                        validateResultPojo.getServiceFees() != null ?
                        validateResultPojo.getServiceFees().toString() : "0";
                    commonBusinessService.insertFileAttachments(requestId,
                                                                fileName,
                                                                openingOfMemorandumsReq.getContentType(),
                                                                ridcPojo.getFileSize(),
                                                                ridcPojo.getAttachDownloadUrl(),
                                                                ridcPojo.getAttachDid(),
                                                                requestId);
                    commonResponse =
                            handleServiceResponse(commonResponse, commonBusinessService,
                                                  openingOfMemorandumsReq.getUserId(),
                                                  serviceFees, requestId,
                                                  false,
                                                  openingOfMemorandumsReq.getServiceId(),
                                                  openingOfMemorandumsReq.getCaseNumber(),
                                                  openingOfMemorandumsReq.getCaseYear(),
                                                  openingOfMemorandumsReq.getProsId(),
                                                  openingOfMemorandumsReq.getInputStream());
                    commonBusinessService.commitAndRelease();
                } else {
                    commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                    commonResponse.setMessage(validateResultPojo.getMessage());
                    // commonResponse.setCanContinue(ConstantsIfc.YES);
                }
            } else {
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.NO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @POST
    @Path("/submitWaiverEnclosureReq")
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitWaiverEnclosureReq(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, CommonRequest waiverEnclosureReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            ResultPojo validateResultPojo = null;
            //                commonBusinessService.validateCase(openingOfMemorandumsReq.getServiceId(),
            //                                                    openingOfMemorandumsReq.getCaseNumber(),
            //                                                    openingOfMemorandumsReq.getCaseYear(),
            //                                                    openingOfMemorandumsReq.getProsId(),
            //                                                    openingOfMemorandumsReq.getEmiratesId(),
            //                                                    openingOfMemorandumsReq.getLocale(), openingOfMemorandumsReq.getCertificateTypeId());
            validateResultPojo = new ResultPojo();
            validateResultPojo.setStatus(ConstantsIfc.VALIDATION_SUCCEEDED);
            if (validateResultPojo != null) {
                if (validateResultPojo.getStatus() != null &&
                    validateResultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.VALIDATION_SUCCEEDED)) {
                    validateResultPojo.setServiceFees(10);
                    String requestId =
                        commonBusinessService.submitWaiverEnclosureReq(waiverEnclosureReq);
                    String fileName =
                        requestId + "_" + waiverEnclosureReq.getFileName();
                    RIDCService ridcService = new RIDCService();
                    RidcPojo ridcPojo =
                        ridcService.uploadedFile(fileName, waiverEnclosureReq.getInputStream(),
                                                 requestId);
                    validateResultPojo.setServiceFees(0);
                    String serviceFees =
                        validateResultPojo.getServiceFees() != null ?
                        validateResultPojo.getServiceFees().toString() : "0";
                    commonBusinessService.insertFileAttachments(requestId,
                                                                fileName,
                                                                waiverEnclosureReq.getContentType(),
                                                                ridcPojo.getFileSize(),
                                                                ridcPojo.getAttachDownloadUrl(),
                                                                ridcPojo.getAttachDid(),
                                                                requestId);
                    commonResponse =
                            handleServiceResponse(commonResponse, commonBusinessService,
                                                  waiverEnclosureReq.getUserId(),
                                                  serviceFees, requestId,
                                                  false,
                                                  waiverEnclosureReq.getServiceId(),
                                                  waiverEnclosureReq.getCaseNumber(),
                                                  waiverEnclosureReq.getCaseYear(),
                                                  waiverEnclosureReq.getProsId(),
                                                  waiverEnclosureReq.getInputStream());
                    commonBusinessService.commitAndRelease();
                } else {
                    commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                    commonResponse.setMessage(validateResultPojo.getMessage());
                    // commonResponse.setCanContinue(ConstantsIfc.YES);
                }
            } else {
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.NO);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @GET
    @Path("/searchCases/{caseNumber}/{locationId}/{caseYear}/{locale}/{relationCase}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchCases(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("caseNumber")
        String caseNumber, @PathParam("locationId")
        String locationId, @PathParam("caseYear")
        String caseYear, @PathParam("locale")
        String locale, @PathParam("relationCase")
        String relationCase) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        CaseDetailsPojo casePojo = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CasesInquiryResponse casesInquiryResponse = new CasesInquiryResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            casePojo =
                    commonBusinessService.searchCases(caseNumber, locationId,
                                                      caseYear, relationCase);
            if (casePojo != null) {
                casesInquiryResponse.setCaseDetails(casePojo);
                casesInquiryResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                casesInquiryResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
            } else {
                casesInquiryResponse.setMessage(ConstantsIfc.NO_DATA);
                casesInquiryResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                casesInquiryResponse.setCanContinue(ConstantsIfc.YES);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            casesInquiryResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            casesInquiryResponse.setCanContinue(ConstantsIfc.NO);
            casesInquiryResponse.setStatusCode(ConstantsIfc.ERROR);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(casesInquiryResponse).build();
    }

    @GET
    @Path("/validateCase/{serviceId}/{caseNumber}/{emiratesId}/{prosId}/{caseYear}/{locale}/{certificateType}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateCase(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("serviceId")
        String serviceId, @PathParam("caseNumber")
        String caseNumber, @PathParam("emiratesId")
        String emiratesId, @PathParam("prosId")
        String prosId, @PathParam("caseYear")
        String caseYear, @PathParam("locale")
        String locale, @PathParam("certificateType")
        String certificateType) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        ResultPojo resultPojo = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            resultPojo =
                    commonBusinessService.validateCase(serviceId, caseNumber,
                                                       caseYear, prosId,
                                                       emiratesId, locale,
                                                       certificateType);
            if (resultPojo != null) {
                commonResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.YES);
                commonResponse.setServiceFees(resultPojo.getServiceFees() !=
                                              null ?
                                              resultPojo.getServiceFees().toString() :
                                              "0");
            } else {
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.YES);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }

    @POST
    @Path("/logInvoiceDetails")
    @Produces(MediaType.APPLICATION_JSON)
    public Response logInvoiceDetails(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, CommonRequest invoiceDetailsReq) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String caseStatus = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CommonResponse commonResponse = new CommonResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            caseStatus =
                    commonBusinessService.logInvoiceDetails(invoiceDetailsReq.getServiceId(),
                                                            invoiceDetailsReq.getTransactionStatus(),
                                                            invoiceDetailsReq.getUserId(),
                                                            invoiceDetailsReq.getRequestId(),
                                                            invoiceDetailsReq.getStatusDetails());
            if (caseStatus != null) {
                commonResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.YES);
                commonBusinessService.handleSendNotifications(invoiceDetailsReq.getUserId(),
                                       invoiceDetailsReq.getRequestId(),
                                       invoiceDetailsReq.getServiceId(), invoiceDetailsReq.getLocale(), "SUBMITTED_SUCCESSFULLY");
            } else {
                commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                commonResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
                // commonResponse.setCanContinue(ConstantsIfc.YES);
            }
            commonBusinessService.commitAndRelease();
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            commonResponse = handleExceptionResponse(commonResponse);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(commonResponse).build();
    }


    @GET
    @Path("/searchRequests/{reqId}/{userId}/{fromDate}/{toDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchRequests(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("reqId")
        String reqId, @PathParam("userId")
        String userId, @PathParam("fromDate")
        String fromDate, @PathParam("toDate")
        String toDate) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        RequestsDetailsResponse requestsDetailsResponse =
            new RequestsDetailsResponse();
        CommonBusinessService commonBusinessService =
            new CommonBusinessService();
        List<RequestDetailsPojo> requestDetailsList =
            new ArrayList<RequestDetailsPojo>();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            requestDetailsList =
                    commonBusinessService.searchRequests(reqId, userId,
                                                         fromDate, toDate);
            //            serviceDetailsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
            if (requestDetailsList != null && requestDetailsList.size() != 0) {
                //                String guid = generateGuid();
                requestsDetailsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                requestsDetailsResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                requestsDetailsResponse.setRequestsDetails(requestDetailsList);
                //                serviceResponse.setGuid(guid);
                //                commonService.createSession(userName, guid);
            } else {
                requestsDetailsResponse.setMessage(ConstantsIfc.NO_DATA);
                requestsDetailsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            requestsDetailsResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            requestsDetailsResponse.setStatusCode(ConstantsIfc.ERROR);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(requestsDetailsResponse).build();
    }

    private CommonResponse handleServiceResponse(CommonResponse commonResponse,
                                                 CommonBusinessService commonBusinessService,
                                                 String userId,
                                                 String serviceFees,
                                                 String requestId,
                                                 boolean reportExistFlag,
                                                 String serviceId,
                                                 String caseNumber,
                                                 String caseYear,
                                                 String prosId,
                                                 String base64String) throws MessagingException,
                                                                             MimeTypeParseException,
                                                                             IOException {
        commonResponse.setServiceFees(serviceFees);
        commonResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
        commonResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
        commonResponse.setRequestId(requestId);
        if (serviceFees != null && !serviceFees.equals("0")) {
            commonResponse.setEDirhamServiceCode(commonBusinessService.getPaymentServiceCode(serviceId,
                                                                                             caseNumber,
                                                                                             caseYear,
                                                                                             prosId,
                                                                                             userId));
            commonResponse.setEDirhamServiceCode("148301-0044");
        }

        commonResponse.setIsReportExist(reportExistFlag);
        //        if (serviceFees != null && serviceFees.equals("0") &&
        //            reportExistFlag == false) {
        //            EmailConfigPojo emailConfigPojo =
        //                commonBusinessService.getEmailConfigDetails();
        //            UserInfoPojo userInfoPojo =
        //                commonBusinessService.getUserDetails(userId);
        //            sendNotifications(userInfoPojo.getMobileNumber(),
        //                              userInfoPojo.getEmailAddress(), requestId,
        //                              serviceId, "en", emailConfigPojo, base64String,
        //                              userInfoPojo.getName());
        //        }
        System.err.println("getEDirhamServiceCode ==>>> " +
                           commonResponse.getEDirhamServiceCode());
        System.err.println("getMessage ==>>> " + commonResponse.getMessage());
        System.err.println("getRequestId ==>>> " +
                           commonResponse.getRequestId());
        System.err.println("getServiceFees ==>>> " +
                           commonResponse.getServiceFees());
        System.err.println("getStatusCode ==>>> " +
                           commonResponse.getStatusCode());
        return commonResponse;
    }

    private CommonResponse handleExceptionResponse(CommonResponse commonResponse) {
        commonResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        // commonResponse.setCanContinue(ConstantsIfc.NO);
        commonResponse.setStatusCode(ConstantsIfc.ERROR);
        return commonResponse;
    }
}
