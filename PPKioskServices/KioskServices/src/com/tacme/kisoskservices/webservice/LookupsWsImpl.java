package com.tacme.kisoskservices.webservice;

import com.tacme.kisoskservices.ifcs.ConstantsIfc;
import com.tacme.kisoskservices.pojos.CertificateTypePojo;
import com.tacme.kisoskservices.pojos.NationalityPojo;
import com.tacme.kisoskservices.pojos.ProsecutionPojo;
import com.tacme.kisoskservices.pojos.RelationshipPojo;
//import com.tacme.kisoskservices.pojos.ServiceDetailsPojo;
import com.tacme.kisoskservices.pojos.AvailableSessionPojo;
import com.tacme.kisoskservices.pojos.ServiceDetailsPojo;
import com.tacme.kisoskservices.response.AvailableSessionsResponse;
import com.tacme.kisoskservices.response.CertificateTypesResponse;
import com.tacme.kisoskservices.response.NationalityResponse;
import com.tacme.kisoskservices.response.ProsecutionResponse;
import com.tacme.kisoskservices.response.RelationshipResponse;
import com.tacme.kisoskservices.response.ServiceDetailsResponse;
import com.tacme.kisoskservices.service.LookupService;
import com.tacme.kisoskservices.util.ContentNegotiationUtil;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;

@Path("json/common/lookups")
@Produces(MediaType.APPLICATION_JSON)

public class LookupsWsImpl {
    public LookupsWsImpl() {
        super();
    }

    @GET
    @Path("/getServiceDetails/{serviceId}/{locale}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServiceDetails(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("serviceId")
        String serviceId, @PathParam("locale")
        String locale) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        ServiceDetailsPojo servicePojo = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        ServiceDetailsResponse serviceDetailsResponse =
            new ServiceDetailsResponse();
        LookupService lookupService = new LookupService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            servicePojo = lookupService.getServiceDetails(serviceId, locale);
//            serviceDetailsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
            if (servicePojo != null && servicePojo.getServiceName() != null) {
                //                String guid = generateGuid();
                serviceDetailsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                serviceDetailsResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                serviceDetailsResponse.setServiceDetails(servicePojo);
                //                serviceResponse.setGuid(guid);
                //                commonService.createSession(userName, guid);
            } else{
                serviceDetailsResponse.setMessage(ConstantsIfc.NO_DATA);
                serviceDetailsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            serviceDetailsResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            serviceDetailsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(serviceDetailsResponse).build();
    }

    @GET
    @Path("/getCertificateTypes/{locale}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCertificateTypes(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("locale")
        String locale) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        List<CertificateTypePojo> certificateTypes = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        CertificateTypesResponse certificateTypesResponse =
            new CertificateTypesResponse();
        LookupService lookupService = new LookupService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            certificateTypes = lookupService.getCertificateTypes(locale);
//            certificateTypesResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
            if (certificateTypes != null && certificateTypes.size() != 0) {
                //                String guid = generateGuid();
                certificateTypesResponse.setCertificateTypes(certificateTypes);
                certificateTypesResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                certificateTypesResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                //                serviceResponse.setGuid(guid);
                //                commonService.createSession(userName, guid);
            } else{
                certificateTypesResponse.setMessage(ConstantsIfc.NO_DATA);
                certificateTypesResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            certificateTypesResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            certificateTypesResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(certificateTypesResponse).build();
    }

    @GET
    @Path("/getNationalities/{locale}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNationalities(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("locale")
        String locale) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        List<NationalityPojo> nationalitiesList = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        NationalityResponse nationalityResponse = new NationalityResponse();
        LookupService lookupService = new LookupService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            nationalitiesList = lookupService.getNationalitiesList(locale);
//            nationalityResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
            if (nationalitiesList != null && nationalitiesList.size() != 0) {
                //                String guid = generateGuid();
                nationalityResponse.setNationalitiesList(nationalitiesList);
                nationalityResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                nationalityResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                //                serviceResponse.setGuid(guid);
                //                commonService.createSession(userName, guid);
            } else {
                nationalityResponse.setMessage(ConstantsIfc.NO_DATA);
                nationalityResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            nationalityResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            nationalityResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(nationalityResponse).build();
    }

    @GET
    @Path("/getProsecutions/{locale}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProsecutions(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("locale")
        String locale) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        List<ProsecutionPojo> prosecutionsList = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        ProsecutionResponse prosecutionResponse = new ProsecutionResponse();
        LookupService lookupService = new LookupService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            prosecutionsList = lookupService.getProsecutionsList(locale);
//            prosecutionResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
            if (prosecutionsList != null && prosecutionsList.size() != 0) {
                //                String guid = generateGuid();
                prosecutionResponse.setProsecutionsList(prosecutionsList);
                prosecutionResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                prosecutionResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                //                serviceResponse.setGuid(guid);
                //                commonService.createSession(userName, guid);
            } else {
                prosecutionResponse.setMessage(ConstantsIfc.NO_DATA);
                prosecutionResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            prosecutionResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            prosecutionResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(prosecutionResponse).build();
    }

    @GET
    @Path("/getPartyRelationships/{locale}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPartyRelationships(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("locale")
        String locale) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        List<RelationshipPojo> relationshipsList = null;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        RelationshipResponse relationshipResponse = new RelationshipResponse();
        LookupService lookupService = new LookupService();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            relationshipsList = lookupService.getPartyRelationships(locale);
//            relationshipResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
            if (relationshipsList != null && relationshipsList.size() != 0) {
                //                String guid = generateGuid();
                relationshipResponse.setRelationshipsList(relationshipsList);
                relationshipResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                relationshipResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                //                serviceResponse.setGuid(guid);
                //                commonService.createSession(userName, guid);
            } else {
                relationshipResponse.setMessage(ConstantsIfc.NO_DATA);
                relationshipResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            relationshipResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            relationshipResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(relationshipResponse).build();
    }

    @GET
    @Path("/getAvailableSessions/{serviceId}/{prosId}/{locale}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAvailableSessions(@Context
        Request restRequest, @Context
        HttpServletRequest httpRequest, @PathParam("serviceId")
        String serviceId, @PathParam("prosId")
        String prosId, @PathParam("locale")
        String locale) {
        int statusCode = ConstantsIfc.RESPONSE_DEFAULT_STATUS_CODE;
        String mediaType = MediaType.APPLICATION_JSON + ";charset=utf-8";
        List<AvailableSessionPojo> availableSessionsList = new ArrayList<AvailableSessionPojo>();
        LookupService lookupService = new LookupService();
        AvailableSessionsResponse availableSessionsResponse = new AvailableSessionsResponse();
        try {
            Variant bestResponseVariant =
                ContentNegotiationUtil.getResponseVariant(restRequest);
            mediaType =
                    ContentNegotiationUtil.getMediaType(bestResponseVariant);
            statusCode =
                    ContentNegotiationUtil.getStatusCode(bestResponseVariant);
            availableSessionsList = lookupService.getAvailableSessions(prosId, serviceId);
//            availableSessionsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
            if (availableSessionsList != null && availableSessionsList.size() != 0) {
                //                String guid = generateGuid();
                availableSessionsResponse.setAvailableSessions(availableSessionsList);
                availableSessionsResponse.setMessage(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                availableSessionsResponse.setStatusCode(ConstantsIfc.RESPONSE_SUCCESS_STATUS);
                //                serviceResponse.setGuid(guid);
                //                commonService.createSession(userName, guid);
            } else {
                availableSessionsResponse.setMessage(ConstantsIfc.NO_DATA);
                availableSessionsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            statusCode = Response.Status.OK.getStatusCode();
            availableSessionsResponse.setMessage(ConstantsIfc.RESPONSE_FAILURE_STATUS);
            availableSessionsResponse.setStatusCode(ConstantsIfc.RESPONSE_FAILURE_STATUS);
        }
        return Response.status(statusCode).type(mediaType).header(" ",
                                                                  statusCode).entity(availableSessionsResponse).build();
    }
}
