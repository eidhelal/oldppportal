package com.tacme.kisoskservices.response;


import com.tacme.kisoskservices.pojos.RelationshipPojo;

import java.util.List;

public class RelationshipResponse {
    private List<RelationshipPojo> relationshipsList;
    private String statusCode;
    private String message;
    
    public RelationshipResponse() {
        super();
    }

    public void setRelationshipsList(List<RelationshipPojo> relationshipsList) {
        this.relationshipsList = relationshipsList;
    }

    public List<RelationshipPojo> getRelationshipsList() {
        return relationshipsList;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
