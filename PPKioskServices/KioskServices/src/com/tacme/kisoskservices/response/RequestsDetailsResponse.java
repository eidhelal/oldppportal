package com.tacme.kisoskservices.response;

import com.tacme.kisoskservices.pojos.RequestDetailsPojo;

import java.util.List;

public class RequestsDetailsResponse {
    private List<RequestDetailsPojo> requestsDetails;
    private String statusCode;
    private String message;
    
    public RequestsDetailsResponse() {
        super();
    }

    public void setRequestsDetails(List<RequestDetailsPojo> requestsDetails) {
        this.requestsDetails = requestsDetails;
    }

    public List<RequestDetailsPojo> getRequestsDetails() {
        return requestsDetails;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
