package com.tacme.kisoskservices.response;

import com.tacme.kisoskservices.pojos.AvailableSessionPojo;

import java.util.List;

public class AvailableSessionsResponse {
    private List<AvailableSessionPojo> availableSessions;
    private String statusCode;
    private String message;
    
    public AvailableSessionsResponse() {
        super();
    }

    public void setAvailableSessions(List<AvailableSessionPojo> availableSessions) {
        this.availableSessions = availableSessions;
    }

    public List<AvailableSessionPojo> getAvailableSessions() {
        return availableSessions;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
