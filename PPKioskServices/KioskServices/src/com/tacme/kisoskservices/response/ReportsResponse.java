package com.tacme.kisoskservices.response;

public class ReportsResponse {
    private String statusCode;
    private String message;
    private String reportResult;
    
    public ReportsResponse() {
        super();
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setReportResult(String reportResult) {
        this.reportResult = reportResult;
    }

    public String getReportResult() {
        return reportResult;
    }
}
