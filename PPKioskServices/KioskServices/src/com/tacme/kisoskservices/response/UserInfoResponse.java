package com.tacme.kisoskservices.response;

import com.tacme.kisoskservices.pojos.UserInfoPojo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@SuppressWarnings("serial")
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class UserInfoResponse {
    
    private UserInfoPojo userInfo;
    private String statusCode;
    private String message;
    
    public UserInfoResponse() {
        super();
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    @JsonProperty("statusCode")
    public String getStatusCode() {
        return statusCode;
    }

    public void setUserInfo(UserInfoPojo userInfo) {
        this.userInfo = userInfo;
    }

    @JsonProperty("userInfo")
    public UserInfoPojo getUserInfo() {
        return userInfo;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }
}
