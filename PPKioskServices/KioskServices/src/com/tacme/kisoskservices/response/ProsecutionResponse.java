package com.tacme.kisoskservices.response;

import com.tacme.kisoskservices.pojos.ProsecutionPojo;

import java.util.List;

public class ProsecutionResponse {
    private List<ProsecutionPojo> prosecutionsList;
    private String statusCode;
    private String message;
    
    public ProsecutionResponse() {
        super();
    }

    public void setProsecutionsList(List<ProsecutionPojo> prosecutionsList) {
        this.prosecutionsList = prosecutionsList;
    }

    public List<ProsecutionPojo> getProsecutionsList() {
        return prosecutionsList;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
