package com.tacme.kisoskservices.response;

import com.tacme.kisoskservices.pojos.CaseDetailsPojo;

import com.tacme.kisoskservices.pojos.CertificateTypePojo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;


@SuppressWarnings("serial")
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class CertificateTypesResponse {
    private List<CertificateTypePojo> certificateTypes;
    private String statusCode;
    private String message;
    
    public CertificateTypesResponse() {
        super();
    }

    public void setCertificateTypes(List<CertificateTypePojo> certificateTypes) {
        this.certificateTypes = certificateTypes;
    }

    public List<CertificateTypePojo> getCertificateTypes() {
        return certificateTypes;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
