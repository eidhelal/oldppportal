package com.tacme.kisoskservices.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
@SuppressWarnings("serial")
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class CommonResponse {
    private String statusCode; // (SUCCESS / FAILURE / ERROR)
    private String message;
    private boolean canContinue;
    private String serviceFees;   // if != 0 then take fees else No Payment required
    private String eDirhamServiceCode; // if paymentRequired then return it else 
    private String requestId;
    private boolean isReportExist; // true or false
    
    public CommonResponse() {
        super();
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("statusCode")
    public String getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    public void setServiceFees(String serviceFees) {
        this.serviceFees = serviceFees;
    }

    @JsonProperty("serviceFees")
    public String getServiceFees() {
        return serviceFees;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    public void setEDirhamServiceCode(String eDirhamServiceCode) {
        this.eDirhamServiceCode = eDirhamServiceCode;
    }

    @JsonProperty("eDirhamServiceCode")
    public String getEDirhamServiceCode() {
        return eDirhamServiceCode;
    }

    public void setIsReportExist(boolean isReportExist) {
        this.isReportExist = isReportExist;
    }

    @JsonProperty("isReportExist")
    public boolean isIsReportExist() {
        return isReportExist;
    }

    public void setCanContinue(boolean canContinue) {
        this.canContinue = canContinue;
    }

    @JsonProperty("canContinue")
    public boolean isCanContinue() {
        return canContinue;
    }
}
