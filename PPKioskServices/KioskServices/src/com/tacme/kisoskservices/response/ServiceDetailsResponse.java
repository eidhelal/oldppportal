package com.tacme.kisoskservices.response;

import com.tacme.kisoskservices.pojos.ServiceDetailsPojo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@SuppressWarnings("serial")
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ServiceDetailsResponse {
    private ServiceDetailsPojo serviceDetails;
    private String statusCode;
    private String message;
    public ServiceDetailsResponse() {
        super();
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setServiceDetails(ServiceDetailsPojo serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    public ServiceDetailsPojo getServiceDetails() {
        return serviceDetails;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
