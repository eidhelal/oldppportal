package com.tacme.kisoskservices.response;

import com.tacme.kisoskservices.pojos.CaseDetailsPojo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@SuppressWarnings("serial")
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class CasesInquiryResponse {
    private CaseDetailsPojo caseDetails;
    private String statusCode;
    private String message;
    private String canContinue;
//    private String serviceFees;   // if != 0 then take fees else No Payment required
//    private String eDirhamServiceCode; // if paymentRequired then return it else 
//    private String requestId;
//    private String isReportExist; // Y or N
    
    public CasesInquiryResponse() {
        super();
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setCanContinue(String canContinue) {
        this.canContinue = canContinue;
    }

    public String getCanContinue() {
        return canContinue;
    }

    public void setCaseDetails(CaseDetailsPojo caseDetails) {
        this.caseDetails = caseDetails;
    }

    public CaseDetailsPojo getCaseDetails() {
        return caseDetails;
    }
}
