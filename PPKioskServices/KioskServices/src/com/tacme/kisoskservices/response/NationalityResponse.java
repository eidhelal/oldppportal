package com.tacme.kisoskservices.response;


import com.tacme.kisoskservices.pojos.NationalityPojo;

import java.util.List;

public class NationalityResponse {
    private List<NationalityPojo> nationalitiesList;
    private String statusCode;
    private String message;
    
    public NationalityResponse() {
        super();
    }

    public void setNationalitiesList(List<NationalityPojo> nationalitiesList) {
        this.nationalitiesList = nationalitiesList;
    }

    public List<NationalityPojo> getNationalitiesList() {
        return nationalitiesList;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
