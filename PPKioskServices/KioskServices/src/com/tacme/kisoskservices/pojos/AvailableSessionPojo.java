package com.tacme.kisoskservices.pojos;

public class AvailableSessionPojo {
    private String prosId;
    private String prosName;
    private String judicialId;
    private String judicialName;
    private String sessionDate;
    private String sessionDayAr;
    private String sessionDayEn;
    private String startTime;
    private String startTimeDescAr;
    private String startTimeDescEn;
    private String endTimeDescAr;
    private String endtTimeDescEn;
    
    public AvailableSessionPojo() {
        super();
    }

    public void setProsId(String prosId) {
        this.prosId = prosId;
    }

    public String getProsId() {
        return prosId;
    }

    public void setProsName(String prosName) {
        this.prosName = prosName;
    }

    public String getProsName() {
        return prosName;
    }

    public void setJudicialId(String judicialId) {
        this.judicialId = judicialId;
    }

    public String getJudicialId() {
        return judicialId;
    }

    public void setJudicialName(String judicialName) {
        this.judicialName = judicialName;
    }

    public String getJudicialName() {
        return judicialName;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDayAr(String sessionDayAr) {
        this.sessionDayAr = sessionDayAr;
    }

    public String getSessionDayAr() {
        return sessionDayAr;
    }

    public void setSessionDayEn(String sessionDayEn) {
        this.sessionDayEn = sessionDayEn;
    }

    public String getSessionDayEn() {
        return sessionDayEn;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTimeDescAr(String startTimeDescAr) {
        this.startTimeDescAr = startTimeDescAr;
    }

    public String getStartTimeDescAr() {
        return startTimeDescAr;
    }

    public void setStartTimeDescEn(String startTimeDescEn) {
        this.startTimeDescEn = startTimeDescEn;
    }

    public String getStartTimeDescEn() {
        return startTimeDescEn;
    }

    public void setEndTimeDescAr(String endTimeDescAr) {
        this.endTimeDescAr = endTimeDescAr;
    }

    public String getEndTimeDescAr() {
        return endTimeDescAr;
    }

    public void setEndtTimeDescEn(String endtTimeDescEn) {
        this.endtTimeDescEn = endtTimeDescEn;
    }

    public String getEndtTimeDescEn() {
        return endtTimeDescEn;
    }
}
