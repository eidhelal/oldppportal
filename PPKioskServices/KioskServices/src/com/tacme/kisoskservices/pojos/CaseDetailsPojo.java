package com.tacme.kisoskservices.pojos;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@SuppressWarnings("serial")
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class CaseDetailsPojo {
    private String caseSubject;
    private String caseNumber;
    private String caseYear;
    private String caseWith;
    private String caseType;
    private String caseStatus;
    private Date caseDate;
    private String caseCategory;
    
    public CaseDetailsPojo() {
        super();
    }

    public void setCaseSubject(String caseSubject) {
        this.caseSubject = caseSubject;
    }

    @JsonProperty("caseSubject")
    public String getCaseSubject() {
        return caseSubject;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    @JsonProperty("caseNumber")
    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseYear(String caseYear) {
        this.caseYear = caseYear;
    }

    @JsonProperty("caseYear")
    public String getCaseYear() {
        return caseYear;
    }

    public void setCaseWith(String caseWith) {
        this.caseWith = caseWith;
    }

    @JsonProperty("caseWith")
    public String getCaseWith() {
        return caseWith;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    @JsonProperty("caseType")
    public String getCaseType() {
        return caseType;
    }

    public void setCaseStatus(String caseStatus) {
        this.caseStatus = caseStatus;
    }

    @JsonProperty("caseStatus")
    public String getCaseStatus() {
        return caseStatus;
    }

    public void setCaseCategory(String caseCategory) {
        this.caseCategory = caseCategory;
    }

    @JsonProperty("caseCategory")
    public String getCaseCategory() {
        return caseCategory;
    }

    public void setCaseDate(Date caseDate) {
        this.caseDate = caseDate;
    }

    @JsonProperty("caseDate")
    public Date getCaseDate() {
        return caseDate;
    }
}
