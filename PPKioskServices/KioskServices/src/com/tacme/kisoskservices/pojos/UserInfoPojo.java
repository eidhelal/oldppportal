package com.tacme.kisoskservices.pojos;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@SuppressWarnings("serial")
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class UserInfoPojo {
    private String userId;
    private String userName;
    private String emailAddress;
    private String name;
    private String emiratesId;
    private String mobileNumber;
    private String dateOfBirth;
//    private String message;
    
    public UserInfoPojo() {
        super();
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("userName")
    public String getUserName() {
        return userName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @JsonProperty("emailAddress")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setEmiratesId(String emiratesId) {
        this.emiratesId = emiratesId;
    }

    @JsonProperty("emiratesId")
    public String getEmiratesId() {
        return emiratesId;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @JsonProperty("mobileNumber")
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @JsonProperty("dateOfBirth")
    public String getDateOfBirth() {
        return dateOfBirth;
    }

//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    @JsonProperty("message")
//    public String getMessage() {
//        return message;
//    }
}
