package com.tacme.kisoskservices.pojos;

public class ProsecutionPojo {
    private String prosecutionCode;
    private String prosecutionNameAr;
    private String prosecutionNameEn;
    public ProsecutionPojo() {
        super();
    }

    public void setProsecutionCode(String prosecutionCode) {
        this.prosecutionCode = prosecutionCode;
    }

    public String getProsecutionCode() {
        return prosecutionCode;
    }

    public void setProsecutionNameAr(String prosecutionNameAr) {
        this.prosecutionNameAr = prosecutionNameAr;
    }

    public String getProsecutionNameAr() {
        return prosecutionNameAr;
    }

    public void setProsecutionNameEn(String prosecutionNameEn) {
        this.prosecutionNameEn = prosecutionNameEn;
    }

    public String getProsecutionNameEn() {
        return prosecutionNameEn;
    }
}
