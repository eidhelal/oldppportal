package com.tacme.kisoskservices.pojos;

public class RidcPojo {
    private String attachDownloadUrl;
    private String attachDid;
    private String fileSize;
    
    public RidcPojo() {
        super();
    }

    public void setAttachDid(String attachDid) {
        this.attachDid = attachDid;
    }

    public String getAttachDid() {
        return attachDid;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setAttachDownloadUrl(String attachDownloadUrl) {
        this.attachDownloadUrl = attachDownloadUrl;
    }

    public String getAttachDownloadUrl() {
        return attachDownloadUrl;
    }
}
