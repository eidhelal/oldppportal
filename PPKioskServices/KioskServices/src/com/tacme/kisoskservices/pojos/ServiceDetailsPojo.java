package com.tacme.kisoskservices.pojos;


import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@SuppressWarnings("serial")
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class ServiceDetailsPojo {
    private String serviceName;
    private String serviceDesc;
    private String submitInDept;
    private String collectFromDept;
    private String serviceFees;
    private String servicePeriod;
    private String requiredDocs;
    private String serviceCondition;
    private String processSummary;
    private String serviceOutput;
    private String otherServiceId;
    
    public ServiceDetailsPojo() {
        super();
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setSubmitInDept(String submitInDept) {
        this.submitInDept = submitInDept;
    }

    public String getSubmitInDept() {
        return submitInDept;
    }

    public void setCollectFromDept(String collectFromDept) {
        this.collectFromDept = collectFromDept;
    }

    public String getCollectFromDept() {
        return collectFromDept;
    }

    public void setServiceFees(String serviceFees) {
        this.serviceFees = serviceFees;
    }

    public String getServiceFees() {
        return serviceFees;
    }

    public void setServicePeriod(String servicePeriod) {
        this.servicePeriod = servicePeriod;
    }

    public String getServicePeriod() {
        return servicePeriod;
    }

    public void setRequiredDocs(String requiredDocs) {
        this.requiredDocs = requiredDocs;
    }

    public String getRequiredDocs() {
        return requiredDocs;
    }

    public void setServiceCondition(String serviceCondition) {
        this.serviceCondition = serviceCondition;
    }

    public String getServiceCondition() {
        return serviceCondition;
    }

    public void setProcessSummary(String processSummary) {
        this.processSummary = processSummary;
    }

    public String getProcessSummary() {
        return processSummary;
    }

    public void setServiceOutput(String serviceOutput) {
        this.serviceOutput = serviceOutput;
    }

    public String getServiceOutput() {
        return serviceOutput;
    }

    public void setOtherServiceId(String otherServiceId) {
        this.otherServiceId = otherServiceId;
    }

    public String getOtherServiceId() {
        return otherServiceId;
    }
}
