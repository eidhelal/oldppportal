package com.tacme.kisoskservices.pojos;

public class RelationshipPojo {
    private String relationshipCode;
    private String relationshipDescAr;
    private String relationshipDescEn;
    
    public RelationshipPojo() {
        super();
    }

    public void setRelationshipCode(String relationshipCode) {
        this.relationshipCode = relationshipCode;
    }

    public String getRelationshipCode() {
        return relationshipCode;
    }

    public void setRelationshipDescAr(String relationshipDescAr) {
        this.relationshipDescAr = relationshipDescAr;
    }

    public String getRelationshipDescAr() {
        return relationshipDescAr;
    }

    public void setRelationshipDescEn(String relationshipDescEn) {
        this.relationshipDescEn = relationshipDescEn;
    }

    public String getRelationshipDescEn() {
        return relationshipDescEn;
    }
}
