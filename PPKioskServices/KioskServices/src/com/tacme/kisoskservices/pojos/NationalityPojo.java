package com.tacme.kisoskservices.pojos;

public class NationalityPojo {
    private String nationalityCode;
    private String nationalityDescAr;
    private String nationalityDescEn;
    
    public NationalityPojo() {
        super();
    }

    public void setNationalityCode(String nationalityCode) {
        this.nationalityCode = nationalityCode;
    }

    public String getNationalityCode() {
        return nationalityCode;
    }

    public void setNationalityDescAr(String nationalityDescAr) {
        this.nationalityDescAr = nationalityDescAr;
    }

    public String getNationalityDescAr() {
        return nationalityDescAr;
    }

    public void setNationalityDescEn(String nationalityDescEn) {
        this.nationalityDescEn = nationalityDescEn;
    }

    public String getNationalityDescEn() {
        return nationalityDescEn;
    }
}
