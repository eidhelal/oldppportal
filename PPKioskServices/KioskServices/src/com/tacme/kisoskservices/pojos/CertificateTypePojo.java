package com.tacme.kisoskservices.pojos;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@SuppressWarnings("serial")
@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)

public class CertificateTypePojo {
    private String certTypeCode;
    private String certTypeDescAr;
    private String certTypeDescEn;
    
    public CertificateTypePojo() {
        super();
    }

    public void setCertTypeCode(String certTypeCode) {
        this.certTypeCode = certTypeCode;
    }

    public String getCertTypeCode() {
        return certTypeCode;
    }

    public void setCertTypeDescAr(String certTypeDescAr) {
        this.certTypeDescAr = certTypeDescAr;
    }

    public String getCertTypeDescAr() {
        return certTypeDescAr;
    }

    public void setCertTypeDescEn(String certTypeDescEn) {
        this.certTypeDescEn = certTypeDescEn;
    }

    public String getCertTypeDescEn() {
        return certTypeDescEn;
    }
}
