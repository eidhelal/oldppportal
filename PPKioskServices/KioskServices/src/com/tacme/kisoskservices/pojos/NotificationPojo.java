package com.tacme.kisoskservices.pojos;

public class NotificationPojo {
    private String message;
    private String subject;
    
    public NotificationPojo() {
        super();
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }
}
