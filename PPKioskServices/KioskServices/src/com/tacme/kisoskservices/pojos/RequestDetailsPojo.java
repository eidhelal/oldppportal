package com.tacme.kisoskservices.pojos;

import java.util.Date;

public class RequestDetailsPojo {
    private String createdDate;
    private String prosNameAr;
    private String prosNameEn;
    private String serviceNameAr;
    private String serviceNameEn;
    private String statusEn;
    private String statusAr;
    public RequestDetailsPojo() {
        super();
    }

    public void setProsNameAr(String prosNameAr) {
        this.prosNameAr = prosNameAr;
    }

    public String getProsNameAr() {
        return prosNameAr;
    }

    public void setProsNameEn(String prosNameEn) {
        this.prosNameEn = prosNameEn;
    }

    public String getProsNameEn() {
        return prosNameEn;
    }

    public void setServiceNameAr(String serviceNameAr) {
        this.serviceNameAr = serviceNameAr;
    }

    public String getServiceNameAr() {
        return serviceNameAr;
    }

    public void setServiceNameEn(String serviceNameEn) {
        this.serviceNameEn = serviceNameEn;
    }

    public String getServiceNameEn() {
        return serviceNameEn;
    }

    public void setStatusEn(String statusEn) {
        this.statusEn = statusEn;
    }

    public String getStatusEn() {
        return statusEn;
    }

    public void setStatusAr(String statusAr) {
        this.statusAr = statusAr;
    }

    public String getStatusAr() {
        return statusAr;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }
}
