package com.tacme.kisoskservices.service;

import com.app.model.services.PPKisokAppServiceImpl;
import com.app.model.utils.PPServicesUtil;

import com.tacme.kisoskservices.ifcs.ServiceIdsIfc;
import com.tacme.kisoskservices.pojos.CertificateTypePojo;
import com.tacme.kisoskservices.pojos.NationalityPojo;
import com.tacme.kisoskservices.pojos.ProsecutionPojo;
import com.tacme.kisoskservices.pojos.RelationshipPojo;
import com.tacme.kisoskservices.pojos.ServiceDetailsPojo;
import com.tacme.kisoskservices.pojos.AvailableSessionPojo;
import com.tacme.kisoskservices.util.ServiceUtil;

import java.util.ArrayList;
import java.util.List;

import oracle.jbo.Row;

public class LookupService {
    public LookupService() {
        super();
    }

    PPKisokAppServiceImpl kisokAppServiceImpl = null;
    
    public List<CertificateTypePojo> getCertificateTypes(String locale) {
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
    //            PPAppServicesImpl ppAppServicesImpl =
    //                (PPAppServicesImpl)kisokAppServiceImpl.getPPAppServices();
            Row[] allCertificateTypesRows =
                kisokAppServiceImpl.getCertificateTypesList(locale);
            if (allCertificateTypesRows != null) {
                List<CertificateTypePojo> certTypesResult = new ArrayList<CertificateTypePojo>();
                for (Row certTypeRow : allCertificateTypesRows) {
                        CertificateTypePojo certificateTypePojo = new CertificateTypePojo();
                        certificateTypePojo.setCertTypeCode(certTypeRow.getAttribute("Code") != null ? certTypeRow.getAttribute("Code").toString() : " ");
                        certificateTypePojo.setCertTypeDescAr(certTypeRow.getAttribute("Description") != null ? certTypeRow.getAttribute("Description").toString() : " ");
                        certificateTypePojo.setCertTypeDescEn(certTypeRow.getAttribute("DescriptionEn") != null ? certTypeRow.getAttribute("DescriptionEn").toString() : " ");
                        certTypesResult.add(certificateTypePojo);
                }
                return certTypesResult;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }
    
    public List<NationalityPojo> getNationalitiesList(String locale) {
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
    //            PPAppServicesImpl ppAppServicesImpl =
    //                (PPAppServicesImpl)kisokAppServiceImpl.getPPAppServices();
            Row[] allCertificateTypesRows =
                kisokAppServiceImpl.getNationalitiesList(locale);
            if (allCertificateTypesRows != null) {
                List<NationalityPojo> nationalitiesResult = new ArrayList<NationalityPojo>();
                for (Row nationalityRow : allCertificateTypesRows) {
                        NationalityPojo nationalityPojo = new NationalityPojo();
                        nationalityPojo.setNationalityCode(nationalityRow.getAttribute("Id") != null ? nationalityRow.getAttribute("Id").toString() : " ");
                        nationalityPojo.setNationalityDescAr(nationalityRow.getAttribute("CountryNameA") != null ? nationalityRow.getAttribute("CountryNameA").toString() : " ");
                        nationalityPojo.setNationalityDescEn(nationalityRow.getAttribute("CountryNameE") != null ? nationalityRow.getAttribute("CountryNameE").toString() : " ");
                        nationalitiesResult.add(nationalityPojo);
                }
                return nationalitiesResult;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }
    
    public List<ProsecutionPojo> getProsecutionsList(String locale) {
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
    //            PPAppServicesImpl ppAppServicesImpl =
    //                (PPAppServicesImpl)kisokAppServiceImpl.getPPAppServices();
            Row[] allProsecutionsRows =
                kisokAppServiceImpl.getProsecutionsList(locale);
            if (allProsecutionsRows != null) {
                List<ProsecutionPojo> prosecutionsResult = new ArrayList<ProsecutionPojo>();
                for (Row prosRow : allProsecutionsRows) {
                        ProsecutionPojo prosecutionPojo = new ProsecutionPojo();
                        prosecutionPojo.setProsecutionCode(prosRow.getAttribute("ProsecutionId") != null ? prosRow.getAttribute("ProsecutionId").toString() : " ");
                        prosecutionPojo.setProsecutionNameAr(prosRow.getAttribute("ProsecutionName") != null ? prosRow.getAttribute("ProsecutionName").toString() : " ");
                        prosecutionPojo.setProsecutionNameEn(prosRow.getAttribute("ProsecutionEngName") != null ? prosRow.getAttribute("ProsecutionEngName").toString() : " ");
                        prosecutionsResult.add(prosecutionPojo);
                }
                return prosecutionsResult;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }
    
    public List<RelationshipPojo> getPartyRelationships(String locale) {
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
    //            PPAppServicesImpl ppAppServicesImpl =
    //                (PPAppServicesImpl)kisokAppServiceImpl.getPPAppServices();
            Row[] allRelationsRows =
                kisokAppServiceImpl.getPartyRelationshipsList(locale);
            if (allRelationsRows != null) {
                List<RelationshipPojo> relationshipsResult = new ArrayList<RelationshipPojo>();
                for (Row relationshipRow : allRelationsRows) {
                        RelationshipPojo relationshipPojo = new RelationshipPojo();
                        relationshipPojo.setRelationshipCode(relationshipRow.getAttribute("Code") != null ? relationshipRow.getAttribute("Code").toString() : " ");
                        relationshipPojo.setRelationshipDescAr(relationshipRow.getAttribute("Description") != null ? relationshipRow.getAttribute("Description").toString() : " ");
                        relationshipPojo.setRelationshipDescEn(relationshipRow.getAttribute("DescriptionEng") != null ? relationshipRow.getAttribute("DescriptionEng").toString() : " ");
                        relationshipsResult.add(relationshipPojo);
                }
                return relationshipsResult;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }


    public List<AvailableSessionPojo> getAvailableSessions(String prosId, String serviceId) {
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            Row[] sessionsRows = 
                kisokAppServiceImpl.getAvailableSessionsList(prosId, getNoOfDays(serviceId));
    //
            List<AvailableSessionPojo> sessionsResult = new ArrayList<AvailableSessionPojo>();
            if (sessionsRows != null && sessionsRows.length != 0) {
                for (Row sessionsRow : sessionsRows) {
                    AvailableSessionPojo sessionPojo = new AvailableSessionPojo();
                    sessionPojo.setEndTimeDescAr(sessionsRow.getAttribute("EndTimeDescAr") !=
                                            null ?
                                            sessionsRow.getAttribute("EndTimeDescAr").toString() :
                                            null);
                    sessionPojo.setEndtTimeDescEn(sessionsRow.getAttribute("EndTimeDescEn") !=
                                           null ?
                                           sessionsRow.getAttribute("EndTimeDescEn").toString() :
                                           null);
                    sessionPojo.setJudicialId(sessionsRow.getAttribute("JudicialId") !=
                                         null ?
                                         sessionsRow.getAttribute("JudicialId").toString() :
                                         null);
                    sessionPojo.setJudicialName(sessionsRow.getAttribute("JudicialName") !=
                                             null ?
                                             sessionsRow.getAttribute("JudicialName").toString() :
                                             null);
                    sessionPojo.setProsId(sessionsRow.getAttribute("ProsecutionId") !=
                                         null ?
                                         sessionsRow.getAttribute("ProsecutionId").toString() :
                                         null);
                    sessionPojo.setProsName(sessionsRow.getAttribute("ProsecutionName") !=
                                         null ?
                                         sessionsRow.getAttribute("ProsecutionName").toString() :
                                         null);
                    sessionPojo.setSessionDate(sessionsRow.getAttribute("SessionDate") !=
                                         null ?
                                         sessionsRow.getAttribute("SessionDate").toString() :
                                         null);
                    sessionPojo.setSessionDayAr(sessionsRow.getAttribute("SessionDayAr") !=
                                           null ?
                                           sessionsRow.getAttribute("SessionDayAr").toString() :
                                           null);
                    sessionPojo.setSessionDayEn(sessionsRow.getAttribute("SessionDayEn") !=
                                           null ?
                                           sessionsRow.getAttribute("SessionDayEn").toString() :
                                           null);
                    sessionPojo.setStartTime(sessionsRow.getAttribute("StartTime") !=
                                           null ?
                                           sessionsRow.getAttribute("StartTime").toString() :
                                           null);
                    sessionPojo.setStartTimeDescAr(sessionsRow.getAttribute("StartTimeDescAr") !=
                                           null ?
                                           sessionsRow.getAttribute("StartTimeDescAr").toString() :
                                           null);
                    sessionPojo.setStartTimeDescEn(sessionsRow.getAttribute("StartTimeDescEn") !=
                                           null ?
                                           sessionsRow.getAttribute("StartTimeDescEn").toString() :
                                           null);
                    sessionsResult.add(sessionPojo);
                }
                return sessionsResult;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public ServiceDetailsPojo getServiceDetails(String serviceId, String locale) {
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
//            PPAppServicesImpl ppAppServicesImpl =
//                (PPAppServicesImpl)kisokAppServiceImpl.getPPAppServices();
            Row[] allServiceRows =
                kisokAppServiceImpl.getServiceDetails(serviceId);
            if(locale.equalsIgnoreCase("ENG"))
                locale = "en";
            else locale = "ar";
            if (allServiceRows != null) {
                ServiceDetailsPojo servicePojo = new ServiceDetailsPojo();
                for (Row serviceRow : allServiceRows) {
                    if (serviceRow.getAttribute("Language") != null &&
                        serviceRow.getAttribute("Language").toString().equalsIgnoreCase(locale)) {
                        servicePojo.setServiceName(serviceRow.getAttribute("Name") !=
                                                   null ?
                                                   serviceRow.getAttribute("Name").toString() :
                                                   " ");
                        servicePojo.setServiceDesc(serviceRow.getAttribute("Description") !=
                                                   null ?
                                                   serviceRow.getAttribute("Description").toString() :
                                                   " ");
                        servicePojo.setSubmitInDept(serviceRow.getAttribute("SubmitInDept") !=
                                                    null ?
                                                    serviceRow.getAttribute("SubmitInDept").toString() :
                                                    " ");
                        servicePojo.setCollectFromDept(serviceRow.getAttribute("CollectFromDept") !=
                                                       null ?
                                                       serviceRow.getAttribute("CollectFromDept").toString() :
                                                       " ");
                        servicePojo.setServiceFees(serviceRow.getAttribute("Fees") !=
                                                   null ?
                                                   serviceRow.getAttribute("Fees").toString() :
                                                   " ");
                        servicePojo.setServicePeriod(serviceRow.getAttribute("PeriodOfService") !=
                                                     null ?
                                                     serviceRow.getAttribute("PeriodOfService").toString() :
                                                     " ");
                        servicePojo.setRequiredDocs(serviceRow.getAttribute("RequiredDocName") !=
                                                    null ?
                                                    serviceRow.getAttribute("RequiredDocName").toString() :
                                                    " ");
                        servicePojo.setServiceCondition(serviceRow.getAttribute("Condition") !=
                                                        null ?
                                                        serviceRow.getAttribute("Condition").toString() :
                                                        " ");
                        servicePojo.setProcessSummary(serviceRow.getAttribute("ProcessSummary") !=
                                                      null ?
                                                      serviceRow.getAttribute("ProcessSummary").toString() :
                                                      " ");
                        servicePojo.setServiceOutput(serviceRow.getAttribute("OutputResult") !=
                                                     null ?
                                                     serviceRow.getAttribute("OutputResult").toString() :
                                                     " ");
                        servicePojo.setServiceName(serviceRow.getAttribute("Name") !=
                                                   null ?
                                                   serviceRow.getAttribute("Name").toString() :
                                                   " ");
                        servicePojo.setOtherServiceId(serviceRow.getAttribute("TranslatedServiceId") !=
                                                   null ?
                                                   serviceRow.getAttribute("TranslatedServiceId").toString() :
                                                   " ");
                    }
                }
                return servicePojo;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    private String getNoOfDays(String serviceId) throws Exception{
        String noOfDays = null;
        if(serviceId.equalsIgnoreCase(ServiceIdsIfc.DECISION_OF_APPEAL_AR) || serviceId.equalsIgnoreCase(ServiceIdsIfc.DECISION_OF_APPEAL_EN))
            noOfDays = "14";
        else if(serviceId.equalsIgnoreCase(ServiceIdsIfc.PROVIDING_OBJECTION_AR) || serviceId.equalsIgnoreCase(ServiceIdsIfc.PROVIDING_OBJECTION_EN))
            noOfDays = "7";
        return noOfDays;
    }
    
    public Row getUcmConfigDetails() throws Exception {
        kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
        Row ucmConfigRow =
            kisokAppServiceImpl.getUcmConfigDetails();
        return ucmConfigRow;
    }
}
