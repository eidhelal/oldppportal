package com.tacme.kisoskservices.service;

import com.app.model.pojos.ResultPojo;
import com.app.model.services.PPKisokAppServiceImpl;
import com.app.model.utils.PPServicesUtil;

import com.tacme.kisoskservices.pojos.CaseDetailsPojo;
import com.tacme.kisoskservices.pojos.CertificateTypePojo;
import com.tacme.kisoskservices.pojos.EmailConfigPojo;
import com.tacme.kisoskservices.pojos.NationalityPojo;
import com.tacme.kisoskservices.pojos.NotificationPojo;
import com.tacme.kisoskservices.pojos.ProsecutionPojo;
import com.tacme.kisoskservices.pojos.RelationshipPojo;
import com.tacme.kisoskservices.pojos.RequestDetailsPojo;
import com.tacme.kisoskservices.pojos.ServiceDetailsPojo;
import com.tacme.kisoskservices.pojos.UserInfoPojo;
import com.tacme.kisoskservices.request.CommonRequest;
import com.tacme.kisoskservices.response.UserInfoResponse;
import com.tacme.kisoskservices.util.EmailUtils;
import com.tacme.kisoskservices.util.ServiceUtil;

import java.io.IOException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

public class CommonBusinessService {
    PPKisokAppServiceImpl kisokAppServiceImpl = null;

    public CommonBusinessService() {
        super();
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
        } catch (Exception e) {
        }
    }

    public String submitCopyOfCaseDroppingReq(CommonRequest copyOfCaseDropingReq) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.insertCopyOfCaseDroping(copyOfCaseDropingReq.getServiceId(),
                                                               copyOfCaseDropingReq.getProsId(),
                                                               copyOfCaseDropingReq.getCaseNumber(),
                                                               copyOfCaseDropingReq.getLocale(),
                                                               copyOfCaseDropingReq.getCaseYear(),
                                                               copyOfCaseDropingReq.getRelationCase(),
                                                               copyOfCaseDropingReq.getUserId(),
                                                               copyOfCaseDropingReq.getAppRef());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public String submitDecisionOfAppealReq(CommonRequest decisionOfAppealReq) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.insertReqDecisionAppeal(decisionOfAppealReq.getServiceId(),
                                                               decisionOfAppealReq.getProsId(),
                                                               decisionOfAppealReq.getCaseNumber(),
                                                               decisionOfAppealReq.getLocale(),
                                                               decisionOfAppealReq.getCaseYear(),
                                                               decisionOfAppealReq.getRelationCase(),
                                                               decisionOfAppealReq.getSelectedSession(),
                                                               decisionOfAppealReq.getUserId(),
                                                               decisionOfAppealReq.getAppRef());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public String submitToWhomItMayConcernReq(CommonRequest toWhomItMayConcernReq) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.insertToWhomItMayConcernRequest(toWhomItMayConcernReq.getServiceId(),
                                                                       toWhomItMayConcernReq.getProsId(),
                                                                       toWhomItMayConcernReq.getCaseNumber(),
                                                                       toWhomItMayConcernReq.getLocale(),
                                                                       toWhomItMayConcernReq.getCaseYear(),
                                                                       toWhomItMayConcernReq.getRelationCase(),
                                                                       toWhomItMayConcernReq.getUserId(),
                                                                       toWhomItMayConcernReq.getAppRef(),
                                                                       toWhomItMayConcernReq.getCertificateTypeId(),
                                                                       toWhomItMayConcernReq.getConcernedParty());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public String subscribeToSmsServiceReq(CommonRequest subscribeToSmsServiceReq) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.subscribeToSmsServiceReq(subscribeToSmsServiceReq.getServiceId(),
                                                                subscribeToSmsServiceReq.getProsId(),
                                                                subscribeToSmsServiceReq.getCaseNumber(),
                                                                subscribeToSmsServiceReq.getLocale(),
                                                                subscribeToSmsServiceReq.getCaseYear(),
                                                                subscribeToSmsServiceReq.getRelationCase(),
                                                                subscribeToSmsServiceReq.getUserId(),
                                                                subscribeToSmsServiceReq.getAppRef(),
                                                                subscribeToSmsServiceReq.getIsSms(),
                                                                subscribeToSmsServiceReq.getIsEmail());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public String submitCopyJudgeReq(CommonRequest copyJudgeReq) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.insertCopyJudgeReq(copyJudgeReq.getServiceId(),
                                                          copyJudgeReq.getProsId(),
                                                          copyJudgeReq.getCaseNumber(),
                                                          copyJudgeReq.getLocale(),
                                                          copyJudgeReq.getCaseYear(),
                                                          copyJudgeReq.getRelationCase(),
                                                          copyJudgeReq.getUserId(),
                                                          copyJudgeReq.getAppRef());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public String submitProvidingAnObjectionReq(CommonRequest providingAnObjectionReq) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.insertProvidingAnObjectionReq(providingAnObjectionReq.getServiceId(),
                                                                     providingAnObjectionReq.getProsId(),
                                                                     providingAnObjectionReq.getCaseNumber(),
                                                                     providingAnObjectionReq.getLocale(),
                                                                     providingAnObjectionReq.getCaseYear(),
                                                                     providingAnObjectionReq.getRelationCase(),
                                                                     providingAnObjectionReq.getUserId(),
                                                                     providingAnObjectionReq.getAppRef(),
                                                                     providingAnObjectionReq.getSelectedSession());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public String submitPayFineReq(CommonRequest payFineReq) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.insertPayFineReq(payFineReq.getServiceId(),
                                                        payFineReq.getProsId(),
                                                        payFineReq.getCaseNumber(),
                                                        payFineReq.getLocale(),
                                                        payFineReq.getCaseYear(),
                                                        payFineReq.getRelationCase(),
                                                        payFineReq.getUserId(),
                                                        payFineReq.getAppRef(),
                                                        payFineReq.getRequestedByName());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public String submitWaiverEnclosureReq(CommonRequest waiverEnclosureReq) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.insertWaiverEnclosureReq(waiverEnclosureReq.getServiceId(),
                                                                waiverEnclosureReq.getProsId(),
                                                                waiverEnclosureReq.getCaseNumber(),
                                                                waiverEnclosureReq.getLocale(),
                                                                waiverEnclosureReq.getCaseYear(),
                                                                waiverEnclosureReq.getRelationCase(),
                                                                waiverEnclosureReq.getUserId(),
                                                                waiverEnclosureReq.getAppRef(),
                                                                waiverEnclosureReq.getAccusedName());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public String submitOpeningOfMemorandumsReq(CommonRequest openingOfMemorandumsReq) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.insertOpeningOfMemReq(openingOfMemorandumsReq.getServiceId(),
                                                             openingOfMemorandumsReq.getProsId(),
                                                             openingOfMemorandumsReq.getCaseNumber(),
                                                             openingOfMemorandumsReq.getLocale(),
                                                             openingOfMemorandumsReq.getCaseYear(),
                                                             openingOfMemorandumsReq.getRelationCase(),
                                                             openingOfMemorandumsReq.getUserId(),
                                                             openingOfMemorandumsReq.getAppRef(),
                                                             openingOfMemorandumsReq.getComplainantName(),
                                                             openingOfMemorandumsReq.getComplainantAddress(),
                                                             openingOfMemorandumsReq.getComplainantMobile(),
                                                             openingOfMemorandumsReq.getComplainantNationalityId(),
                                                             openingOfMemorandumsReq.getMemoSubject(),
                                                             openingOfMemorandumsReq.getMemoCategory());
            //            insertFileAttachments(requestId, fileName, contentType, attachSize, attachDownloadUrl, attachDid, userId, kisokAppServiceImpl);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            //             // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public CaseDetailsPojo searchCases(String caseNo, String locationId,
                                       String caseYear, String relationCase) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            //            PPAppServicesImpl ppAppServicesImpl =
            //                (PPAppServicesImpl)kisokAppServiceImpl.getPPAppServices();
            Row caseRows =
                kisokAppServiceImpl.searchCases(caseNo, locationId, caseYear,
                                                relationCase);
            if (caseRows != null) {
                CaseDetailsPojo casePojo = new CaseDetailsPojo();
                casePojo.setCaseSubject(caseRows.getAttribute("Subject") !=
                                        null ?
                                        caseRows.getAttribute("Subject").toString() :
                                        null);
                casePojo.setCaseNumber(caseRows.getAttribute("Casenumber") !=
                                       null ?
                                       caseRows.getAttribute("Casenumber").toString() :
                                       null);
                casePojo.setCaseDate(caseRows.getAttribute("Casedate") !=
                                     null ?
                                     (Date)caseRows.getAttribute("Casedate") :
                                     null);
                casePojo.setCaseCategory(caseRows.getAttribute("Casecategory") !=
                                         null ?
                                         caseRows.getAttribute("Casecategory").toString() :
                                         null);
                casePojo.setCaseType(caseRows.getAttribute("Casetype") !=
                                     null ?
                                     caseRows.getAttribute("Casetype").toString() :
                                     null);
                casePojo.setCaseYear(caseRows.getAttribute("Caseyear") !=
                                     null ?
                                     caseRows.getAttribute("Caseyear").toString() :
                                     null);
                casePojo.setCaseWith(caseRows.getAttribute("Casewith") !=
                                     null ?
                                     caseRows.getAttribute("Casewith").toString() :
                                     null);
                casePojo.setCaseStatus(caseRows.getAttribute("Casestatusid") !=
                                       null ?
                                       caseRows.getAttribute("Casestatusid").toString() :
                                       null);
                return casePojo;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public ResultPojo validateCase(String serviceId, String caseNumber,
                                   String caseYear, String prosId,
                                   String emiratesId, String locale,
                                   String certificateType) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            //            PPAppServicesImpl ppAppServicesImpl =
            //                (PPAppServicesImpl)kisokAppServiceImpl.getPPAppServices();
            ResultPojo resultPojo =
                kisokAppServiceImpl.validatingCase(serviceId, emiratesId,
                                                   caseNumber, prosId,
                                                   caseYear, locale,
                                                   certificateType);
            //            if(resultPojo.getStatus() != null && resultPojo.getStatus().equalsIgnoreCase(ConstantsIfc.RESPONSE_SUCCESS_STATUS))
            return resultPojo;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
    }

    public String getServiceFees(String serviceId) throws Exception {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            return kisokAppServiceImpl.getServiceFees(serviceId, null);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
    }

    public String logInvoiceDetails(String serviceId, String transactionStatus,
                                    String userId, String requestId,
                                    String statusDetails) throws Exception {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            kisokAppServiceImpl.logInvoiceDetails(serviceId, transactionStatus,
                                                  userId, requestId,
                                                  statusDetails);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
//            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return "";
    }

    public String insertFileAttachments(String requestId, String fileName,
                                        String contentType, String attachSize,
                                        String attachDownloadUrl,
                                        String attachDid, String userId) {
        try {
            //            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            kisokAppServiceImpl.insertFileAttachments(requestId, fileName,
                                                      contentType, attachSize,
                                                      attachDownloadUrl,
                                                      attachDid, userId);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            // PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public void commitAndRelease() {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            kisokAppServiceImpl.getDBTransaction().commit();
            System.err.println("committed");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
            System.err.println("Released");
        }
    }

    public List<RequestDetailsPojo> searchRequests(String reqId, String userId,
                                                   String fromDate,
                                                   String toDate) {
        try {
            // kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            Row[] requestsRows =
                kisokAppServiceImpl.searchRequests(reqId, userId,
                                                   fromDate != null ?
                                                   ServiceUtil.asJboDate(ServiceUtil.asUtilDate(fromDate)) :
                                                   null,
                                                   toDate != null ? ServiceUtil.asJboDate(ServiceUtil.asUtilDate((toDate))) :
                                                   null);
            if (requestsRows != null) {
                List<RequestDetailsPojo> requestsList =
                    new ArrayList<RequestDetailsPojo>();
                for (Row reqRow : requestsRows) {
                    RequestDetailsPojo reqDetailsPojo =
                        new RequestDetailsPojo();
                    oracle.jbo.domain.Date dateDomain =
                        (oracle.jbo.domain.Date)reqRow.getAttribute("CreatedDate");
                    reqDetailsPojo.setCreatedDate(reqRow.getAttribute("CreatedDate") !=
                                                  null ?
                                                  new SimpleDateFormat("dd-MM-yyyy").format(dateDomain.getValue()) :
                                                  null);
                    reqDetailsPojo.setProsNameAr(reqRow.getAttribute("ProsArName") !=
                                                 null ?
                                                 reqRow.getAttribute("ProsArName").toString() :
                                                 null);
                    reqDetailsPojo.setProsNameEn(reqRow.getAttribute("ProsEnName") !=
                                                 null ?
                                                 reqRow.getAttribute("ProsEnName").toString() :
                                                 null);
                    reqDetailsPojo.setServiceNameAr(reqRow.getAttribute("ServiceNameAr") !=
                                                    null ?
                                                    reqRow.getAttribute("ServiceNameAr").toString() :
                                                    null);
                    reqDetailsPojo.setServiceNameEn(reqRow.getAttribute("ServiceNameEn") !=
                                                    null ?
                                                    reqRow.getAttribute("ServiceNameEn").toString() :
                                                    null);
                    reqDetailsPojo.setStatusAr(reqRow.getAttribute("Status") !=
                                               null ?
                                               reqRow.getAttribute("Status").toString() :
                                               null);
                    reqDetailsPojo.setStatusEn(reqRow.getAttribute("StatusAr") !=
                                               null ?
                                               reqRow.getAttribute("StatusAr").toString() :
                                               null);
                    requestsList.add(reqDetailsPojo);
                }
                return requestsList;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }


    private Date getDate(String dateStr) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //    Calendar cal = Calendar.getInstance();
        //    long t = cal.getTimeInMillis();
        //    java.util.Date currUtilDate = new java.util.Date(t);
        String dateStr2 = dateFormat.format(dateStr.toString());
        java.util.Date utilDate = dateFormat.parse(dateStr2);
        System.err.println("utilDate ==>> " + utilDate);
        return utilDate;
    }

    public UserInfoPojo getUserDetails(String userId) {
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            Row userRow = kisokAppServiceImpl.getUserDetails(userId);
            if (userRow != null && userRow.getAttribute("Id") != null) {
                UserInfoPojo userInfoPojo = new UserInfoPojo();
                userInfoPojo.setUserId(userRow.getAttribute("Id").toString());
                userInfoPojo.setEmailAddress((String)userRow.getAttribute("Email"));
                userInfoPojo.setDateOfBirth(userRow.getAttribute("DateOfBirth") !=
                                            null ?
                                            userRow.getAttribute("DateOfBirth").toString() :
                                            null);

                userInfoPojo.setName(userRow.getAttribute("FirstName") + " " +
                                     userRow.getAttribute("LastName"));
                userInfoPojo.setEmiratesId(userRow.getAttribute("EmiratesId") !=
                                           null ?
                                           userRow.getAttribute("EmiratesId").toString() :
                                           null);
                userInfoPojo.setMobileNumber(userRow.getAttribute("Mobile") !=
                                             null ?
                                             userRow.getAttribute("Mobile").toString() :
                                             null);
                return userInfoPojo;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            //            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
        return null;
    }

    public String getPaymentServiceCode(String serviceId, String caseNumber,
                                        String caseYear, String prosId,
                                        String userId) {
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            String eDirhamServiceCode =
                kisokAppServiceImpl.getEdirhamServiceCode(serviceId,
                                                          caseNumber, caseYear,
                                                          prosId, userId);
            return eDirhamServiceCode;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            //                PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
    }

    public EmailConfigPojo getEmailConfigDetails() {
        try {
            //                kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            EmailConfigPojo emailConfigPojo = new EmailConfigPojo();
            Row emailConfigRow = kisokAppServiceImpl.getEmailConfigDetails();
            if (emailConfigRow != null) {
                emailConfigPojo.setHost(emailConfigRow.getAttribute("EcHost") !=
                                        null ?
                                        emailConfigRow.getAttribute("EcHost").toString() :
                                        null);
                emailConfigPojo.setPassword(emailConfigRow.getAttribute("EcPwrd") !=
                                            null ?
                                            emailConfigRow.getAttribute("EcPwrd").toString() :
                                            null);
                emailConfigPojo.setPortNumber(emailConfigRow.getAttribute("EcPort") !=
                                              null ?
                                              emailConfigRow.getAttribute("EcPort").toString() :
                                              null);
                emailConfigPojo.setSendUser(emailConfigRow.getAttribute("EcSndMail") !=
                                            null ?
                                            emailConfigRow.getAttribute("EcSndMail").toString() :
                                            null);
                emailConfigPojo.setUserName(emailConfigRow.getAttribute("EcUser") !=
                                            null ?
                                            emailConfigRow.getAttribute("EcUser").toString() :
                                            null);
            }
            return emailConfigPojo;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            //                PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
    }

    public NotificationPojo getNotificationMessage(String locale, String stepCode) {
        try {
            //                kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
            Row notificationRow = kisokAppServiceImpl.getNotificationMessage(locale,
                                                              stepCode);
            NotificationPojo notificationPojo = new NotificationPojo();
            notificationPojo.setMessage(notificationRow.getAttribute("Message") != null ? notificationRow.getAttribute("Message").toString() : null);
            notificationPojo.setSubject(notificationRow.getAttribute("Subject") != null ? notificationRow.getAttribute("Subject").toString() : null);
            return notificationPojo;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            //                PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
    }

    public void handleSendNotifications(String userId, String requestId,
                                         String serviceId,
                                         String locale, String stepCode) throws MessagingException,
                                                               IOException {
        if (serviceId != null && !serviceId.equalsIgnoreCase("1") &&
            !serviceId.equalsIgnoreCase("1001") &&
            !serviceId.equalsIgnoreCase("4") &&
            !serviceId.equalsIgnoreCase("1004") &&
            !serviceId.equalsIgnoreCase("7") &&
            !serviceId.equalsIgnoreCase("1007")) {
            EmailConfigPojo emailConfigPojo = getEmailConfigDetails();
            UserInfoPojo userInfoPojo = getUserDetails(userId);
            LookupService lookupService = new LookupService();
            ServiceDetailsPojo serviceDetailsPojo =
                lookupService.getServiceDetails(serviceId, locale);
            String serviceName = serviceDetailsPojo.getServiceName();
            NotificationPojo notificationPojo = getNotificationMessage(locale, stepCode);
            String subject = notificationPojo.getSubject().replace("${requestNo}", requestId);
            String message = notificationPojo.getMessage().replace("${requestNo}", requestId);
            message = message.replace("${serviceName}", serviceName).replace("${applicantName}", userInfoPojo.getName());
            System.err.println("message ==>> " + message + " and subject ==>> " + subject);
//            sendNotifications(userInfoPojo.getMobileNumber(),
//                              userInfoPojo.getEmailAddress(), requestId,
//                              locale, emailConfigPojo, null,
//                              userInfoPojo.getName(), serviceName, message);
            
            sendEmail(requestId, userInfoPojo.getEmailAddress(), locale, emailConfigPojo,
                      null, userInfoPojo.getName(), serviceName, message, subject);
            executeSMSproc(userInfoPojo.getMobileNumber(), message);
        }
    }

    public String sendEmail(String requestId, String emailAddress,
                            String locale, EmailConfigPojo emailConfigPojo,
                            String base64String, String applicantName,
                            String serviceName,
                            String message, String subject) throws MessagingException,
                                                   IOException {
        EmailUtils.sendEmail(emailAddress, subject, message,
                             emailConfigPojo, base64String, requestId,
                             serviceName);
        return null;
    }

    public void executeSMSproc(String mobileNumber, String message) {
                String subj = null;
                try {
                    kisokAppServiceImpl.insertSmsRow(mobileNumber, message);
//                    for (int i = 0; i <= 1; i++) {
        //                if (i == 0) {
        //                    subj =
        //    "Dear User, Your request is now submitted and it is under processing . Your request number is : " +
        //    requestId;
        //                } else if (i == 1) {
        //                    subj =
        //    "عزيزي المستخدم طلبك تحت قيد التن�?يذ . رقم طلبك :" + requestId;
        //                }
        //                DBTransactionImpl dbti =
        //                    (DBTransactionImpl)getAm().getDBTransaction();
        //                CallableStatement statement =
        //                    dbti.createCallableStatement(("BEGIN " +
        //                                                  "SENDSMS_PORTAL(?,?);" +
        //                                                  "END;"), 0);
        //                try {
        //                    statement.setString(1, p_mobno);
        //                    statement.setString(2, subj);
        //                    statement.execute();
        //                } catch (SQLException sqlerr) {
        //                    throw new JboException(sqlerr);
        //                } finally {
        //                    try {
        //                        if (statement != null) {
        //                            statement.close();
        //                        }
        //                    } catch (SQLException closeerr) {
        //                        throw new JboException(closeerr);
        //                    }
        //                }
        //            }
                } catch (Exception e) {
                    e.printStackTrace();
                }
    }
}
