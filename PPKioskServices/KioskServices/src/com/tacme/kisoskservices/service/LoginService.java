package com.tacme.kisoskservices.service;

import com.app.model.pojos.ResultPojo;
import com.app.model.services.PPAppServicesImpl;
import com.app.model.services.PPKisokAppServiceImpl;
import com.app.model.utils.PPServicesUtil;

import com.tacme.kisoskservices.ifcs.ConstantsIfc;
import com.tacme.kisoskservices.pojos.CaseDetailsPojo;
import com.tacme.kisoskservices.pojos.ServiceDetailsPojo;
import com.tacme.kisoskservices.pojos.AvailableSessionPojo;
import com.tacme.kisoskservices.pojos.UserInfoPojo;

import com.tacme.kisoskservices.util.ServiceUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.util.List;

import oracle.jbo.Row;

public class LoginService {
    public LoginService() {
        super();
    }

    PPKisokAppServiceImpl kisokAppServiceImpl = null;

    public UserInfoPojo login(String userName, String password,
                              String dateOfBirth) {
        try {
            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
//            PPAppServicesImpl ppAppServicesImpl =
//                (PPAppServicesImpl)kisokAppServiceImpl.getPPAppServices();
            Row userRow = kisokAppServiceImpl.login(userName, password);
            return getUserPojo(userRow, dateOfBirth);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            System.err.println("in finally");
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
    }

    public UserInfoPojo login(String emiratesId, String dateOfBirth) {
        try {

            kisokAppServiceImpl = ServiceUtil.getKioskServiceAppModule();
//            PPAppServicesImpl ppAppServicesImpl =
//                (PPAppServicesImpl)kisokAppServiceImpl.getPPAppServices();
            Row userRow = kisokAppServiceImpl.login(emiratesId);
            return getUserPojo(userRow, dateOfBirth);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            PPServicesUtil.releaseSelfCareModule(kisokAppServiceImpl, true);
        }
    }

    private UserInfoPojo getUserPojo(Row userRow, String dateOfBirth) {
        UserInfoPojo userInfoPojo = new UserInfoPojo();
        if (userRow != null) {
//            Date usrDOB =
//                userRow.getAttribute("DateOfBirth") != null ? ServiceUtil.asUtilDate(new oracle.jbo.domain.Date(userRow.getAttribute("DateOfBirth").toString())) :
//                null;
//            String userDateOfBirth = ServiceUtil.formatDate(usrDOB);
//            if (userDateOfBirth != null && dateOfBirth != null)
//                if (dateOfBirth.equalsIgnoreCase(userDateOfBirth)) {
                    userInfoPojo.setUserId(userRow.getAttribute("Id").toString());
                    userInfoPojo.setEmailAddress((String)userRow.getAttribute("Email"));
                    userInfoPojo.setDateOfBirth(userRow.getAttribute("DateOfBirth") !=
                                                null ? dateOfBirth : null);

                    userInfoPojo.setName(userRow.getAttribute("FirstName") +
                                         " " +
                                         userRow.getAttribute("LastName"));
                    userInfoPojo.setEmiratesId(userRow.getAttribute("EmiratesId") !=
                                               null ?
                                               userRow.getAttribute("EmiratesId").toString() :
                                               null);
                    userInfoPojo.setMobileNumber(userRow.getAttribute("Mobile") !=
                                                 null ?
                                                 userRow.getAttribute("Mobile").toString() :
                                                 null);
                    return userInfoPojo;
                }
//        else {
//                    userInfoPojo.setMessage(ConstantsIfc.DATE_OF_BIRTH_MISMATCH);
//                }
//        } 
//    else {
//            userInfoPojo.setMessage(ConstantsIfc.NO_DATA);
//        }
        return userInfoPojo;
    }
}
