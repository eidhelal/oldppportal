package com.app.model.views;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Aug 22 15:28:48 GST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class LoginUserViewImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public LoginUserViewImpl() {
    }

    /**
     * Returns the bind variable value for em.
     * @return bind variable value for em
     */
    public String getem() {
        return (String)getNamedWhereClauseParam("em");
    }

    /**
     * Sets <code>value</code> for bind variable em.
     * @param value value to bind as em
     */
    public void setem(String value) {
        setNamedWhereClauseParam("em", value);
    }

    /**
     * Returns the bind variable value for pwrd.
     * @return bind variable value for pwrd
     */
    public String getpwrd() {
        return (String)getNamedWhereClauseParam("pwrd");
    }

    /**
     * Sets <code>value</code> for bind variable pwrd.
     * @param value value to bind as pwrd
     */
    public void setpwrd(String value) {
        setNamedWhereClauseParam("pwrd", value);
    }

    /**
     * Returns the bind variable value for p_st.
     * @return bind variable value for p_st
     */
    public String getp_st() {
        return (String)getNamedWhereClauseParam("p_st");
    }

    /**
     * Sets <code>value</code> for bind variable p_st.
     * @param value value to bind as p_st
     */
    public void setp_st(String value) {
        setNamedWhereClauseParam("p_st", value);
    }
}
