package com.app.model.views.kiosk;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Aug 23 10:20:07 GST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CopyOfJudgeRequestsCountROVOImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public CopyOfJudgeRequestsCountROVOImpl() {
    }

    /**
     * Returns the bind variable value for pUserId.
     * @return bind variable value for pUserId
     */
    public String getpUserId() {
        return (String)getNamedWhereClauseParam("pUserId");
    }

    /**
     * Sets <code>value</code> for bind variable pUserId.
     * @param value value to bind as pUserId
     */
    public void setpUserId(String value) {
        setNamedWhereClauseParam("pUserId", value);
    }

    /**
     * Returns the bind variable value for pProsId.
     * @return bind variable value for pProsId
     */
    public String getpProsId() {
        return (String)getNamedWhereClauseParam("pProsId");
    }

    /**
     * Sets <code>value</code> for bind variable pProsId.
     * @param value value to bind as pProsId
     */
    public void setpProsId(String value) {
        setNamedWhereClauseParam("pProsId", value);
    }

    /**
     * Returns the bind variable value for pCaseNumber.
     * @return bind variable value for pCaseNumber
     */
    public String getpCaseNumber() {
        return (String)getNamedWhereClauseParam("pCaseNumber");
    }

    /**
     * Sets <code>value</code> for bind variable pCaseNumber.
     * @param value value to bind as pCaseNumber
     */
    public void setpCaseNumber(String value) {
        setNamedWhereClauseParam("pCaseNumber", value);
    }

    /**
     * Returns the bind variable value for pCaseYear.
     * @return bind variable value for pCaseYear
     */
    public String getpCaseYear() {
        return (String)getNamedWhereClauseParam("pCaseYear");
    }

    /**
     * Sets <code>value</code> for bind variable pCaseYear.
     * @param value value to bind as pCaseYear
     */
    public void setpCaseYear(String value) {
        setNamedWhereClauseParam("pCaseYear", value);
    }
}
