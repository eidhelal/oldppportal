package com.app.model.views.kiosk;

import oracle.jbo.domain.Number;
import oracle.jbo.domain.RowID;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Aug 16 14:20:35 GST 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class NationalitiesROVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        Id {
            public Object get(NationalitiesROVORowImpl obj) {
                return obj.getId();
            }

            public void put(NationalitiesROVORowImpl obj, Object value) {
                obj.setId((Number)value);
            }
        }
        ,
        CountryNameA {
            public Object get(NationalitiesROVORowImpl obj) {
                return obj.getCountryNameA();
            }

            public void put(NationalitiesROVORowImpl obj, Object value) {
                obj.setCountryNameA((String)value);
            }
        }
        ,
        CountryNameE {
            public Object get(NationalitiesROVORowImpl obj) {
                return obj.getCountryNameE();
            }

            public void put(NationalitiesROVORowImpl obj, Object value) {
                obj.setCountryNameE((String)value);
            }
        }
        ,
        NationalityNameA {
            public Object get(NationalitiesROVORowImpl obj) {
                return obj.getNationalityNameA();
            }

            public void put(NationalitiesROVORowImpl obj, Object value) {
                obj.setNationalityNameA((String)value);
            }
        }
        ,
        NationalityNameE {
            public Object get(NationalitiesROVORowImpl obj) {
                return obj.getNationalityNameE();
            }

            public void put(NationalitiesROVORowImpl obj, Object value) {
                obj.setNationalityNameE((String)value);
            }
        }
        ,
        Rowid1 {
            public Object get(NationalitiesROVORowImpl obj) {
                return obj.getRowid1();
            }

            public void put(NationalitiesROVORowImpl obj, Object value) {
                obj.setRowid1((RowID)value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(NationalitiesROVORowImpl object);

        public abstract void put(NationalitiesROVORowImpl object,
                                 Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int ID = AttributesEnum.Id.index();
    public static final int COUNTRYNAMEA = AttributesEnum.CountryNameA.index();
    public static final int COUNTRYNAMEE = AttributesEnum.CountryNameE.index();
    public static final int NATIONALITYNAMEA = AttributesEnum.NationalityNameA.index();
    public static final int NATIONALITYNAMEE = AttributesEnum.NationalityNameE.index();
    public static final int ROWID1 = AttributesEnum.Rowid1.index();

    /**
     * This is the default constructor (do not remove).
     */
    public NationalitiesROVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Id.
     * @return the Id
     */
    public Number getId() {
        return (Number) getAttributeInternal(ID);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Id.
     * @param value value to set the  Id
     */
    public void setId(Number value) {
        setAttributeInternal(ID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CountryNameA.
     * @return the CountryNameA
     */
    public String getCountryNameA() {
        return (String) getAttributeInternal(COUNTRYNAMEA);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CountryNameA.
     * @param value value to set the  CountryNameA
     */
    public void setCountryNameA(String value) {
        setAttributeInternal(COUNTRYNAMEA, value);
    }

    /**
     * Gets the attribute value for the calculated attribute CountryNameE.
     * @return the CountryNameE
     */
    public String getCountryNameE() {
        return (String) getAttributeInternal(COUNTRYNAMEE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute CountryNameE.
     * @param value value to set the  CountryNameE
     */
    public void setCountryNameE(String value) {
        setAttributeInternal(COUNTRYNAMEE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute NationalityNameA.
     * @return the NationalityNameA
     */
    public String getNationalityNameA() {
        return (String) getAttributeInternal(NATIONALITYNAMEA);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute NationalityNameA.
     * @param value value to set the  NationalityNameA
     */
    public void setNationalityNameA(String value) {
        setAttributeInternal(NATIONALITYNAMEA, value);
    }

    /**
     * Gets the attribute value for the calculated attribute NationalityNameE.
     * @return the NationalityNameE
     */
    public String getNationalityNameE() {
        return (String) getAttributeInternal(NATIONALITYNAMEE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute NationalityNameE.
     * @param value value to set the  NationalityNameE
     */
    public void setNationalityNameE(String value) {
        setAttributeInternal(NATIONALITYNAMEE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Rowid1.
     * @return the Rowid1
     */
    public RowID getRowid1() {
        return (RowID) getAttributeInternal(ROWID1);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Rowid1.
     * @param value value to set the  Rowid1
     */
    public void setRowid1(RowID value) {
        setAttributeInternal(ROWID1, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index,
                                           AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value,
                                         AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}
