package com.app.model.utils;

import com.app.model.services.PPKisokAppServiceImpl;
import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import oracle.adf.share.ADFContext;
import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

public class PPServicesUtil {
    public PPServicesUtil() {
        super();
    }
    
    private static PPKisokAppServiceImpl getCommonService(String config) {
    //        System.out.println("*****************************************in UAQ GET COMMMON SERVICE iNSTANCE");
        try {
            ADFContext currentADFContext = null;
            if (ADFContext.getCurrent() == null)
                currentADFContext =
                        ADFContext.initADFContext(null, null, null, null);
            String amDef =
                "com.app.model.services.PPKisokAppService";
            ApplicationModule am =
                Configuration.createRootApplicationModule(amDef, config);
            PPKisokAppServiceImpl service = (PPKisokAppServiceImpl)am;
            //        Configuration.releaseRootApplicationModule(appMod, false);

            return service;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Used for creating pooling CommonServiceImpl object
     * @return
     */
    public static PPKisokAppServiceImpl getCommonService() {
        return getCommonService("PPKisokCommonInstance");
    }

    /**
     * Used for creating non pooling CommonServiceImpl object
     * @return
     */
    public static PPKisokAppServiceImpl getCommonServiceInstance() {
        return getCommonService("PPKisokCommonInstance");
    }

    /**
     * Release CommonServiceImpl object from connection pool
     *
     * @param appMod
     * @param remove
     */
    public static void releaseSelfCareModule(ApplicationModule appMod,
                                             boolean remove) {
        System.err.println("in releaseSelfCareModule");
        Configuration.releaseRootApplicationModule(appMod, remove);
    }

    public static Connection getConnection() throws NamingException,
                                                    SQLException {
        Context ctx = new InitialContext();
        return ((DataSource)ctx.lookup("jdbc/ppdevDS")).getConnection();

        //        try {
        //            Class.forName("oracle.jdbc.driver.OracleDriver");
        //        } catch (ClassNotFoundException e) {
        //        }
        //        Connection con =
        //            DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe",
        //                                        "uaq_services", "uaq_services");
        //        return con;
    }
}
