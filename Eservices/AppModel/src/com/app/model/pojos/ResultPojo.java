package com.app.model.pojos;

import java.io.Serializable;

public class ResultPojo implements Serializable{

    private String status;
    private String message;
    private Number serviceFees;
    
    public ResultPojo() {
        super();
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setServiceFees(Number serviceFees) {
        this.serviceFees = serviceFees;
    }

    public Number getServiceFees() {
        return serviceFees;
    }
}
