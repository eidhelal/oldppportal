package com.tacme.pp.common.utils;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;

import org.apache.commons.codec.binary.Base64;


/**
 * @author pritam 
 * AESEncription on given key
 * Cipher.getInstance("AES/CBC/PKCS5PADDING")
 */
public class AESEncription implements Serializable{



	public static String encrypt(String value)
			throws IllegalBlockSizeException, BadPaddingException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			UnsupportedEncodingException, InvalidKeyException,
			InvalidAlgorithmParameterException {

		String key = "Bar12345Bar12345";
		String initVector = "RandomInitVector";
		IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipher.init(1, skeySpec, iv);

		byte[] encrypted = cipher.doFinal(value.getBytes());
		System.out.println("encrypted string: "
				+ new String(Base64.encodeBase64(encrypted)));

		//return Base64.encodeBase64(encrypted);
                
		return new  String (Base64.encodeBase64(encrypted));

	}

	public static byte[] decrypt(String encrypted) throws InvalidKeyException,
			InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, UnsupportedEncodingException {

		
		
		String key = "Bar12345Bar12345";
		String initVector = "RandomInitVector";
		IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipher.init(2, skeySpec, iv);

		byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted.getBytes()));

		System.out.println(new String(original));

		return original;

	}
	
	public static void main(String[] args) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException{
		encrypt("130");
		//decrypt("wBF+CQHZYEuxTvnhziAQkA==");
	    //String encodedUrl = URLEncoder.encode("uaEOjVbDUx201D5ilqJ+HQ==", "UTF-8");
            //System.out.println(encodedUrl);
	    
	}

}
