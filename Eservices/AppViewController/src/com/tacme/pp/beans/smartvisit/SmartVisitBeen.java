package com.tacme.pp.beans.smartvisit;

import com.app.view.beans.UCMBean;

import com.tacme.pp.common.utils.ADFUtils;

import com.tacme.pp.common.utils.JSFUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.SQLException;

import java.text.DecimalFormat;

import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.share.ADFContext;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.input.RichInputFile;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.Number;

import org.apache.commons.io.IOUtils;
import org.apache.myfaces.trinidad.model.UploadedFile;

public class SmartVisitBeen {
    private List<UploadedFile> uploadedfile;
    private RichInputFile upFileBind;
    private String cn = "N";
    private String pc = "N";
    private String cy = "N";
    private String rc = "N";
    private String inmate = "N";
    private String vd = "N";
    private String vp = "N";
    private String firstVisit;
    private static final String VALID_FILE = "Y";
    private static final String NOt_VALID_FILE = "N";
    private static final String WEB_APP = "web-app";
    private static final String REQ_NAME = "Request of Smart Visit";
    private static final String REQ_STATUS = "SENT";

    private static final ADFLogger logger =
        ADFLogger.createADFLogger(SmartVisitBeen.class);
    private RichPanelGroupLayout panelGroupBinding;

    public SmartVisitBeen() {
        super();
    }

    public Locale getTheCurrentLocale() {
        Locale mylocale = new Locale("ar");
        try {
            mylocale = (Locale)ADFUtils.evaluateEL("#{pageFlowScope.locale}");
        } catch (Exception e) {
            mylocale = new Locale("ar");
        }
        return mylocale;
    }

    public String submitAction() {
        String s = "Y";
        String res = "ok";

        Locale localel = getTheCurrentLocale();
        String locale = localel.toString();


        //        ADFUtils.setEL("#{pageFlowScope.ServiceId}",
        //                       ((Number)reqr.getAttribute("ServiceId")).stringValue());
        try {
            java.util.List<UploadedFile> fileList = this.getUploadedfile();

            String key = (String)ADFUtils.evaluateEL("#{pageFlowScope.key}");
            //  String res = this.validatingCase(sid, caseno, name, year);
            if (firstVisit.equals("true")) {
                if (ADFUtils.getBoundAttributeValue("CaseNumber") != null &&
                    ADFUtils.getBoundAttributeValue("CaseYear") != null &&
                    ADFUtils.getBoundAttributeValue("ProsecId") != null &&
                    ADFUtils.getBoundAttributeValue("RelashionshipCode") !=
                    null &&
                    ADFUtils.getBoundAttributeValue("InmateNo") != null) {

                    if (!res.equals("ok"))
                        return "notfound"; 

                } else {
                    if (ADFUtils.getBoundAttributeValue("CaseNumber") ==
                        null) {
                        this.setCn("Y");
                    }
                    if (ADFUtils.getBoundAttributeValue("CaseYear") == null) {
                        this.setCy("Y");
                    }
                    if (ADFUtils.getBoundAttributeValue("ProsecId") == null) {
                        this.setPc("Y");
                    }
                    if (ADFUtils.getBoundAttributeValue("RelashionshipCode") ==
                        null) {
                        this.setRc("Y");
                    }
                    if (ADFUtils.getBoundAttributeValue("InmateNo") == null) {
                        this.setInmate("Y");
                    }
                    if(ADFUtils.getBoundAttributeValue("VisitDate") == null)
                        this.setVd("Y");
                    if( ADFUtils.getBoundAttributeValue("VisitReason") == null)
                        this.setVp("Y");
                    
                    return null;
                }
            }else {
                
                    if (ADFUtils.getBoundAttributeValue("InmateNoList") == null) {
                        this.setInmate("Y");
                        if(ADFUtils.getBoundAttributeValue("VisitDate") == null)
                            this.setVd("Y");
                        if( ADFUtils.getBoundAttributeValue("VisitReason") == null)
                            this.setVp("Y");
                        return null;
                    }
                
                }
            if (ADFUtils.getBoundAttributeValue("VisitDate") == null ||
                ADFUtils.getBoundAttributeValue("VisitReason") == null ){
                if(ADFUtils.getBoundAttributeValue("VisitDate") == null)
                    this.setVd("Y");
                if( ADFUtils.getBoundAttributeValue("VisitReason") == null)
                    this.setVp("Y");
                    return null;
                }
            
            
            //        if (s.equals("Y")) {
            //
            //            sentEmail((Number)reqr.getAttribute("ReqId"),
            //                      setServiceNames(locale, key), locale);
            //
            //            oracle.jbo.domain.Number csid = (Number)reqr.getAttribute("ReqId");
            //            executeSMSproc((String)ADFContext.getCurrent().getSessionScope().get("mobile"),
            //                           csid);
            //            ADFUtils.setEL("#{pageFlowScope.ReqId}",
            //                           ((Number)reqr.getAttribute("ReqId")).stringValue());
            //            ADFUtils.setEL("#{pageFlowScope.ServiceId}",
            //                           ((Number)reqr.getAttribute("ServiceId")).stringValue());
            //            return "successRating";
            //        }
            if (uploadFileList(fileList).equalsIgnoreCase(NOt_VALID_FILE))
                return null;
           
            JSFUtils.setExpressionValue("#{pageFlowScope.ReqId}",ADFUtils.getBoundAttributeValue("ReqId"));      
            ADFUtils.setBoundAttributeValue("ReqName", REQ_NAME);
            ADFUtils.setBoundAttributeValue("Status", REQ_STATUS);
            ADFUtils.setBoundAttributeValue("CreatedDate",
                                            this.getCurrentOracleDateTime());
            ADFUtils.setBoundAttributeValue("PublicUserId",
                                            ADFContext.getCurrent().getSessionScope().get("uid"));
            ADFUtils.setBoundAttributeValue("ServiceId",
                                            new Number(JSFUtils.resolveExpression("#{pageFlowScope.key}")));
            ADFUtils.setBoundAttributeValue("AppRef", WEB_APP);
            OperationBinding operationBinding =
                ADFUtils.findOperation("Commit");
            operationBinding.execute();
            if (!operationBinding.getErrors().isEmpty()) {
                ADFUtils.findOperation("Rollback").execute();
                return "showError";
            }
            
        } catch (Throwable e) {
            logger.severe(e.getMessage(), e);
            OperationBinding rollbackOP = ADFUtils.findOperation("Rollback");
            rollbackOP.execute();
            return "showError";
        }
        
        return "success";
    }

    private static oracle.jbo.domain.Date getCurrentOracleDateTime() {
        return new oracle.jbo.domain.Date(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    private String uploadFile_caseCopy(UploadedFile file, String did) {
        String fileName = null;
        String contentType = null;
        BlobDomain blob = null;
        String fileSize = null;
        DecimalFormat df2 = new DecimalFormat(".###");
        fileName = file.getFilename();
        contentType = file.getContentType();
        blob = getBlob(file);
        double l = file.getLength();
        double kb = l / 1024;
        double mb = kb / 1024;
        fileSize = df2.format(kb) + " kB";
        if (mb < 10) {
            copy1FileUploadaction(did, fileName, contentType, fileSize);
        } else {
            FacesMessage Message = null;
            if ((getTheCurrentLocale().toString()).equals("en")) {
                Message =
                        new FacesMessage("File Size should be less than 10 MB");
            }
            if ((getTheCurrentLocale().toString()).equals("ar")) {
                Message =
                        new FacesMessage("يجب ان يكون المل�? أقَل من 10 ميجا بايت ");
            }
            Message.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage(null, Message);
            return NOt_VALID_FILE;
        }
        return VALID_FILE;
    }

    private String copy1FileUploadaction(String did, String fileName,
                                         String contentType, String fileSize) {
        UCMBean ubean = new UCMBean();
        ViewObject reqAttachmentsView =
            ADFUtils.findIterator("ReqAttachmentsView1Iterator").getViewObject();
        Row rar = reqAttachmentsView.createRow();
        rar.setAttribute("DisplayName", fileName);
        rar.setAttribute("ContentType", contentType);
        //        rar.setAttribute("Attachment", blob);
        rar.setAttribute("AttachSize", fileSize);
        rar.setAttribute("AttachDownloadUrl", ubean.downloadAttachUrl());
        reqAttachmentsView.insertRow(rar);
        Number attachid = (Number)rar.getAttribute("Id");
        this.insert_did(attachid, did);
        return null;
    }

    /**
     * Insert into UCM_COnfig info table
     * @return
     */
    public void insert_did(Number reqattach, String docid) {
        ViewObject attachucmView =
            ADFUtils.findIterator("AttachUcmInfoView1Iterator").getViewObject();
        Row attachUCMRow = attachucmView.createRow();
        attachUCMRow.setAttribute("RaId", reqattach);
        attachUCMRow.setAttribute("UcmDocId", docid);
        attachucmView.insertRow(attachUCMRow);
    }

    private BlobDomain getBlob(UploadedFile file) {
        InputStream in = null;
        BlobDomain blobDomain = null;
        OutputStream out = null;
        try {
            in = file.getInputStream();
            blobDomain = new BlobDomain();
            out = blobDomain.getBinaryOutputStream();
            IOUtils.copy(in, out);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.fillInStackTrace();
        }
        return blobDomain;
    }

    public String validateFileUpload(List<UploadedFile> fileList) {
        String validateFile = null;

        if (fileList != null) {
            for (int i = 0; i < fileList.size(); i++) {
                if ((fileList.get(i).getFilename().toLowerCase().endsWith("jpeg")) ||
                    (fileList.get(i).getFilename().toLowerCase().endsWith("jpg")) ||
                    (fileList.get(i).getFilename().toLowerCase().endsWith("txt")) ||
                    (fileList.get(i).getFilename().toLowerCase().endsWith("doc")) ||
                    (fileList.get(i).getFilename().toLowerCase().endsWith("docx")) ||
                    (fileList.get(i).getFilename().toLowerCase().endsWith("gif")) ||
                    (fileList.get(i).getFilename().toLowerCase().endsWith("pdf")) ||
                    (fileList.get(i).getFilename().toLowerCase().endsWith("png")) ||
                    (fileList.get(i).getFilename().toLowerCase().endsWith("tiff"))) {

                    validateFile = VALID_FILE;

                } else {
                    validateFile = NOt_VALID_FILE;
                    break;
                }
            }
        }
        return validateFile;
    }


    public void setUploadedfile(List<UploadedFile> uploadedfile) {
        this.uploadedfile = uploadedfile;
    }

    public List<UploadedFile> getUploadedfile() {
        return uploadedfile;
    }

    public void setUpFileBind(RichInputFile upFileBind) {
        this.upFileBind = upFileBind;
    }

    public RichInputFile getUpFileBind() {
        return upFileBind;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getCn() {
        return cn;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getPc() {
        return pc;
    }

    public void setCy(String cy) {
        this.cy = cy;
    }

    public String getCy() {
        return cy;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public String getRc() {
        return rc;
    }

    private String uploadFileList(List<UploadedFile> fileList) {
        UCMBean ubean = new UCMBean();
        String returnValue = VALID_FILE;
        if (fileList != null) {
            String validateFile = validateFileUpload(fileList);
            if (validateFile.equals(VALID_FILE)) {
                for (int i = 0; i < fileList.size(); i++) {
                    String folderName =
                        ADFUtils.getBoundAttributeValue("ReqId").toString();
                    if (i == 0) {
                        ubean.createFolder(folderName);
                    }
                    ubean.uploadedFile(folderName, fileList.get(i),
                                       folderName);
                    System.out.println("--------------->>>>>>>>>>>>" +
                                       uploadFile_caseCopy(fileList.get(i),
                                                           ubean.getDid()));
                    if (uploadFile_caseCopy(fileList.get(i),
                                            ubean.getDid()).equals(NOt_VALID_FILE)) {
                        returnValue = NOt_VALID_FILE;
                        break;
                    }
                }
            } else {

                if ((getTheCurrentLocale().toString()).equals("en")) {
                    FacesContext.getCurrentInstance().addMessage(upFileBind.getClientId(FacesContext.getCurrentInstance()),
                                                                 new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                                  "File Type extension are not allowed",
                                                                                  "Kindly reload the files"));
                }
                if ((getTheCurrentLocale().toString()).equals("ar")) {
                    FacesContext.getCurrentInstance().addMessage(upFileBind.getClientId(FacesContext.getCurrentInstance()),
                                                                 new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                                                  "نوع المل�? غير مسموح به",
                                                                                  "يرجى اعادة ر�?ع المل�?ات بالانواع المسموح بيها "));
                }
                returnValue = NOt_VALID_FILE;
            }
        }
        return returnValue;
    }

    public void setInmate(String inmate) {
        this.inmate = inmate;
    }

    public String getInmate() {
        return inmate;
    }

    public void setVp(String vp) {
        this.vp = vp;
    }

    public String getVp() {
        return vp;
    }

    public void setVd(String vd) {
        this.vd = vd;
    }

    public String getVd() {
        return vd;
    }

    public void setFirstVisit(String firstVisit) {
        this.firstVisit = firstVisit;
    }

    public String getFirstVisit() {
        if (firstVisit == null) {
            firstVisit = "true";
        }
        return firstVisit;
    }

    public void checkVisitVCL(ValueChangeEvent valueChange) {
        // Add event code here...
        Object newValue = valueChange.getNewValue();
        if (newValue != null) {
            if (firstVisit.equals("false")) {
                ADFUtils.setBoundAttributeValue("CaseNumber", null);
                ADFUtils.setBoundAttributeValue("ProsecId", null);
                ADFUtils.setBoundAttributeValue("CaseYear", null);
                ADFUtils.setBoundAttributeValue("RelashionshipCode", null);
                ADFUtils.setBoundAttributeValue("InmateNo", null);
            } else {
             //   ADFUtils.setBoundAttributeValue("InmateNoList", null);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(panelGroupBinding);
    }

    public void setPanelGroupBinding(RichPanelGroupLayout panelGroupBinding) {
        this.panelGroupBinding = panelGroupBinding;
    }

    public RichPanelGroupLayout getPanelGroupBinding() {
        return panelGroupBinding;

    }
}
