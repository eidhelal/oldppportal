package com.app.view;

import com.app.model.views.PuPublicuserViewImpl;

import com.app.model.views.PuPublicuserViewRowImpl;

import com.tacme.pp.common.utils.AESEncription;

import com.tacme.pp.common.utils.ADFUtils;

import com.tacme.pp.common.utils.JSFUtils;

import com.tacme.pp.common.utils.EmailUtils;

import java.io.UnsupportedEncodingException;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.OperationBinding;

import oracle.jbo.domain.DBSequence;

import java.net.URLEncoder;

import javax.faces.validator.ValidatorException;

public class ForgotPasswordBean {
    private RichInputText searchEmailId;

    public ForgotPasswordBean() {
    }

    public void setSearchEmailId(RichInputText searchEmailId) {
        this.searchEmailId = searchEmailId;
    }

    public RichInputText getSearchEmailId() {
        return searchEmailId;
    }

    public String searchOnEmail() {
        // Add event code here...
        String emailSearch = searchEmailId.getValue().toString();
        System.out.println("Start-" + emailSearch);

        DCIteratorBinding puPublicuserView1Iterator =
            ADFUtils.findIterator("PuPublicuserViewWithCriteraIterator");


        PuPublicuserViewImpl puPublicuserView =
            (PuPublicuserViewImpl)puPublicuserView1Iterator.getViewObject();

        OperationBinding binding = ADFUtils.findOperation("ExecuteWithParams");
        binding.getParamsMap().put("pEmailId", emailSearch);
        binding.execute();

        PuPublicuserViewRowImpl row =
            (PuPublicuserViewRowImpl)puPublicuserView.first();

        if (row == null) {
          
            System.out.println("No Record");
            //JSFUtils.addFacesErrorMessage(JSFUtils.resolveExpressionAsString("#{appviewcontrollerbundle.email_notfound}"));
            String errormsg =
                JSFUtils.resolveExpressionAsString("#{appviewcontrollerbundle.email_notfound}");
            FacesMessage fMsg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, errormsg,
                                 errormsg);
            //  JSFUtils.addFacesErrorMessage(errormsg);

            FacesContext.getCurrentInstance().addMessage(searchEmailId.getClientId(FacesContext.getCurrentInstance()),
                                                         fMsg);
            return null;
            //  throw new ValidatorException(fMsg);

            // ADFUtils.setBoundAttributeValue(errormsg, errormsg);
        } else {
            System.out.println(" Record found -" + row.getEmail());
            ADFUtils.findOperation("CreateInsert").execute();

            ADFUtils.setBoundAttributeValue("UserId", row.getEmail());
            ADFUtils.setBoundAttributeValue("IsExpiry", "N");

            OperationBinding commitOP = ADFUtils.findOperation("Commit");
            commitOP.execute();
            
            if (!commitOP.getErrors().isEmpty()) {
                Throwable t = (Throwable)commitOP.getErrors().get(0);

                OperationBinding rollbackOP =
                    ADFUtils.findOperation("Rollback");
                rollbackOP.execute();
                return null;
            }

            DBSequence id = (DBSequence)ADFUtils.getBoundAttributeValue("Id");
            String sequence = id.getSequenceNumber().stringValue();

            try {
                String encriptedId = AESEncription.encrypt(sequence);
                System.out.println(" sequence Id--" + sequence);

                String encriptedIdURLENCOADE =
                    URLEncoder.encode(encriptedId, "UTF-8");
                System.out.println(" encriptedIdURLENCOADE Id--" +
                                   encriptedIdURLENCOADE);
                String url =
                    JSFUtils.resolveExpressionAsString("#{WCAppContext.applicationURL}");
                String text =
                    url + "/portal/pp/EnterPassword?link=" + encriptedIdURLENCOADE;

                String subject =
                    JSFUtils.resolveExpressionAsString("#{appviewcontrollerbundle.PASSWORD_MAIL_SUBJECT}");
                    
                String clickHere =
                    JSFUtils.resolveExpressionAsString("#{appviewcontrollerbundle.CLICK_HERE}");
                
                String htmlBody =
                    "<html><body><h3>" +JSFUtils.resolveExpressionAsString("#{appviewcontrollerbundle.HELLO_USER}")+"&nbsp;&nbsp;"+ row.getFirstName() + "</h3><br/><br/><div>" +
                    JSFUtils.resolveExpressionAsString("#{appviewcontrollerbundle.CLICK_LINK}") +
                    "<br/><a style ='display: inline-block;\n" + 
                    "    margin-bottom: 0;\n" + 
                    "    text-align: center;\n" + 
                    "    vertical-align: middle;\n" + 
                    "    cursor: pointer;\n" + 
                    "    white-space: nowrap;\n" + 
                    "    -webkit-user-select: none;\n" + 
                    "    -moz-user-select: none;\n" + 
                    "    -ms-user-select: none;\n" + 
                    "    -o-user-select: none;\n" + 
                    "    user-select: none;\n" + 
                    "    color: #ffffff;\n" + 
                    "    -webkit-border-radius: 3px;\n" + 
                    "    -moz-border-radius: 3px;\n" + 
                    "    -ms-border-radius: 3px;\n" + 
                    "    -o-border-radius: 3px;\n" + 
                    "    -khtml-border-radius: 3px;\n" + 
                    "    border-radius: 3px;\n" + 
                    "    background-color: #af2222 !important;\n" + 
                    "    background-image: none;\n" + 
                    "    font-family: Arial, Helvetica, sans-serif;\n" + 
                    "    font-size: 16px;\n" + 
                    "    line-height: 6px;\n" + 
                    "    padding: 15px 10px 15px 10px !important;\n" + 
                    "    margin: 15px 0px 15px 10px !important;\n" + 
                    "    width: 190px;\n" + 
                    "    border: 0px none;\n" + 
                    "    background-image: none !important;\n ' href ="+text+">"+clickHere+"</a></div></body></html>";
                     


                EmailUtils.sendHTMLEmail(row.getEmail(), subject, htmlBody);


            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            System.out.println(" sequence Id--" + sequence + "  email-" +
                               row.getEmail());

            
        }


        return "success";
    }
}
