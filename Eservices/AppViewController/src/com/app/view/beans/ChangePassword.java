package com.app.view.beans;

import com.app.model.views.PuPublicuserViewRowImpl;
import com.app.model.views.Pu_ForgotPasswordVOImpl;

import com.app.model.views.Pu_ForgotPasswordVORowImpl;

import com.tacme.pp.common.utils.ADFUtils;

import com.tacme.pp.common.utils.JSFUtils;

import java.io.Serializable;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.OperationBinding;

import oracle.jbo.RowSetIterator;

import org.apache.myfaces.trinidad.util.ComponentReference;

public class ChangePassword implements Serializable{
    private ComponentReference password;
    private ComponentReference confirmPassword;

    public ChangePassword() {
    }

  
    
    public void setPassword(RichInputText password) {
        this.password = ComponentReference.newUIComponentReference(password);
        
    }

    public RichInputText getPassword() {
        if (password!=null)
          {
            return (RichInputText) password.getComponent();
          }
          return null; 

    }

    public void setConfirmPassword(RichInputText confirmPassword) {
        this.confirmPassword = ComponentReference.newUIComponentReference(confirmPassword);
        
    }

    public RichInputText getConfirmPassword() {
        if (confirmPassword!=null)
          {
            return (RichInputText) confirmPassword.getComponent();
          }
          return null; 

    }
 

    public String changePassword() {

        String linkId =
            JSFUtils.resolveExpressionAsString("#{pageFlowScope.linkid}");

        System.out.println("from Url-" + linkId);


        DCIteratorBinding Pu_ForgotPasswordVO =
            ADFUtils.findIterator("Pu_ForgotPasswordVOIterator");
        System.out.println(Pu_ForgotPasswordVO.getAllRowsInRange().length);
        RowSetIterator iterator = Pu_ForgotPasswordVO.getRowSetIterator();

        Pu_ForgotPasswordVORowImpl pu_ForgotPasswordVORow =
            (Pu_ForgotPasswordVORowImpl)iterator.first();

        if (pu_ForgotPasswordVORow != null) {

            System.out.println(pu_ForgotPasswordVORow.getId() + "-email-" +
                               pu_ForgotPasswordVORow.getUserId());


            OperationBinding binding =
                ADFUtils.findOperation("ExecuteWithParams1");
            binding.getParamsMap().put("pEmailId",
                                       pu_ForgotPasswordVORow.getUserId());
            binding.execute();

            DCIteratorBinding PuPublicuserView1Iterator =
                ADFUtils.findIterator("PuPublicuserViewWithCriteraIterator");

            System.out.println(PuPublicuserView1Iterator.getAllRowsInRange().length);

            PuPublicuserViewRowImpl puPublicuserViewRow =
                (PuPublicuserViewRowImpl)PuPublicuserView1Iterator.getViewObject().getCurrentRow();

            if (puPublicuserViewRow != null) {
                System.out.println(puPublicuserViewRow.getId() + "-email-" +
                                   puPublicuserViewRow.getPassword());
                puPublicuserViewRow.setPassword(getPassword().getValue().toString());

                pu_ForgotPasswordVORow.setIsExpiry("Y");
            }
            OperationBinding commitOP = ADFUtils.findOperation("Commit");
            commitOP.execute();

            if (!commitOP.getErrors().isEmpty()) {
                Throwable t = (Throwable)commitOP.getErrors().get(0);

                OperationBinding rollbackOP =
                    ADFUtils.findOperation("Rollback");
                rollbackOP.execute();
                return "showError";
            }
        }

        return "thankyou";
    }

    public void valueChangePassword(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        getConfirmPassword().resetValue();
        getConfirmPassword().setValue("");
        oracle.adf.view.rich.context.AdfFacesContext.getCurrentInstance().addPartialTarget(getConfirmPassword());
    }


    public void confirmPasswordValidator(FacesContext facesContext,
                                         UIComponent uIComponent,
                                         Object object) {
        String passowrdConfirmation = (String)object;
        String passowrd = getPassword().getValue().toString();

        if (passowrd != null && !passowrd.equals(passowrdConfirmation)) {
            String msg =JSFUtils.resolveExpressionAsString("#{appviewcontrollerbundle.password_mismatch}");
                //JSFUtils.resolveExpressionAsString("#{uaqBundle.password_mismatch}");
            javax.faces.application.FacesMessage fMsg =
                new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.SEVERITY_ERROR,
                                                         msg, msg);
            throw new javax.faces.validator.ValidatorException(fMsg);
        }
    }
}
